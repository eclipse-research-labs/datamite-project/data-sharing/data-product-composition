# Copyright (c) 2024 Centre for Research and Technology Hellas (CERTH) / Information Technologies Institute (ITI)
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
# 
# SPDX-License-Identifier: MIT
# 
# Contributors:
#     Nikolaos Tepelidis - Author
#     Vasileios Siopidis - Couthor
#     Georgios Nikolaidis - Couthor
#     Konstantinos Votis - Couthor


import sys
from flask import jsonify

def analyze_datasets_logic(datasets, filtered_dfs, merged_dataset_by_columns):
    """
    Function to analyze uploaded datasets, filtered datasets, and the merged dataset, and return results.
    """
    try:
        results = []  # Initialize an empty list to store analysis results

        # Analyze imported datasets
        for i, data in enumerate(datasets):  
            title = data['title']
            size = data['size']  
            df = data['dataframe']  



            field_names = df.columns.tolist()  
            field_types = df.dtypes.apply(lambda x: x.name).to_dict()  
            entries_count = df.count().to_dict()  

            result = {
                "title": title,
                "size": size,
                "dataset_index": i + 1,
                "field_names": field_names,
                "field_types": field_types,
                "entries_count": entries_count
            }
            results.append(result)

        # Analyze filtered datasets
        for i, (title, filtered_df) in enumerate(filtered_dfs.items(), start=len(datasets) + 1):
            size = filtered_df.memory_usage(index=True).sum() / 1024  # Size in KB
            field_names = filtered_df.columns.tolist()  
            field_types = filtered_df.dtypes.apply(lambda x: x.name).to_dict()  
            entries_count = filtered_df.count().to_dict()  

            result = {
                "title": f"filtered_{title}",
                "size": round(size, 2),  # Round size to 2 decimal places
                "dataset_index": i,
                "field_names": field_names,
                "field_types": field_types,
                "entries_count": entries_count
            }
            results.append(result)

        # Analyze merged dataset if available
        if merged_dataset_by_columns is not None:
            size = merged_dataset_by_columns.memory_usage(index=True).sum() / 1024  # Size in KB
            field_names = merged_dataset_by_columns.columns.tolist()  
            field_types = merged_dataset_by_columns.dtypes.apply(lambda x: x.name).to_dict()  
            entries_count = merged_dataset_by_columns.count().to_dict()  

            result = {
                "title": "merged_dataset",
                "size": round(size, 2),  
                "dataset_index": len(results) + 1,
                "field_names": field_names,
                "field_types": field_types,
                "entries_count": entries_count
            }
            results.append(result)

        return jsonify(results), 200  # Return the analysis results as JSON response

    except Exception as e:  
        return jsonify({'status': 'error', 'message': str(e)}), 500