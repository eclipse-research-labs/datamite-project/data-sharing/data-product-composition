# Copyright (c) 2024 Centre for Research and Technology Hellas (CERTH) / Information Technologies Institute (ITI)
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
# 
# SPDX-License-Identifier: MIT
# 
# Contributors:
#     Nikolaos Tepelidis - Author
#     Vasileios Siopidis - Couthor
#     Georgios Nikolaidis - Couthor
#     Konstantinos Votis - Couthor

import psycopg2
from flask import Flask, request, jsonify, session, url_for
from flask import Response
from flask_swagger_ui import get_swaggerui_blueprint  # Importing Swagger UI blueprint
import sys # Importing sys module for system-specific parameters and functions
import pandas as pd # Importing pandas library for data manipulation and analysis
from werkzeug.utils import secure_filename # Importing secure_filename function from werkzeug module
import tempfile # Importing tempfile module for creating temporary files and directories
import os # Importing os module for interacting with the operating system
from io import StringIO # Importing StringIO module for working with text in memory
#from aggregate_columns_by_columns import merge_datasets
from flask import send_file
from flask import make_response # Importing send_file and make_response functions from flask
import io  # Importing io module for working with streams
from io import BytesIO
import nltk
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
from nltk.stem import WordNetLemmatizer
import unicodedata
import re
import numpy as np # Used for numerical operations
import json # Used for handling JSON data
import csv
import logging
import uuid
nltk.download('stopwords')
nltk.download('punkt')
nltk.download('wordnet')
from sqlalchemy.exc import SQLAlchemyError
from sqlalchemy import create_engine, MetaData, Table, Column, String, DateTime, Text, Integer
from sqlalchemy.sql import func, and_ , or_
from datetime import datetime
import json
from sqlalchemy import select, join, cast, String, LargeBinary
from sqlalchemy import ARRAY
from upload_insert_datasets import upload_dataset, get_dataset, retrieve_data_product
from analyze import analyze_datasets_logic
from merge_datasets_by_columns_rows import show_datasets_logic, merge_datasets_bycolumns_logic, download_merged_dataset_bycolumns_logic, merge_datasets_byrows_logic, download_merged_dataset_byrows_logic, merge_datasets_byPrimaryKey_logic, download_merged_dataset_byPrimaryKey_logic
from delete_retain import get_all_datasets_logic, delete_columns_logic, download_deleted_dataset_logic, retain_columns_logic, download_dataset_logic
from filter_columns import get_datasets_logic, download_filtered_logic
from download_data_product import download_data_product_logic
from werkzeug.exceptions import RequestEntityTooLarge
#from save_update_delete_database import save_data_product_logic, save_merged_dataset_to_db, update_data_product_logic, save_metadata, update_metadata_logic, get_metadata_logic, get_anonymized_data_logic
from reset_datasets_file import reset_datasets_logic
from sqlalchemy.dialects.postgresql import JSONB
engine = None  # Your database engine
from sqlalchemy.orm import sessionmaker
temp_storage = {} 
import requests
import zipfile
app = Flask(__name__) # Creating a Flask app instance

@app.after_request
def add_cors_headers(response):
    response.headers["Access-Control-Allow-Origin"] = "*"  # Allow all origins
    response.headers["Access-Control-Allow-Methods"] = "GET, POST, PUT, DELETE, OPTIONS"  # Allow these methods
    response.headers["Access-Control-Allow-Headers"] = "Content-Type, Authorization, X-Requested-With"  # Allow these headers
    return response


datasets = [] # Initializing an empty list to store uploaded datasets
uploaded_file_format = None # Initializing a variable to store the uploaded file format

logging.basicConfig(level=logging.DEBUG)

# Define the database URI

DATABASE_URI = 'postgresql://postgres:1234@postgres:5432/anonymization_tool'

try:
    # Create the database engine
    engine = create_engine(DATABASE_URI)
    factory = sessionmaker(bind=engine)
    session = factory()
    
    # Test the connection by attempting to connect and execute a query
    with engine.connect() as connection:
        print("Database connection established successfully")
        
        
except SQLAlchemyError as e:
    print(f"Database connection error: {str(e)}")

# Define table schema

metadata = MetaData()

data_product_dataset_table = Table(
    'data_product_dataset', metadata,
    Column('unique_id', String, primary_key=True),
    Column('data', JSONB),
    Column('zip_data', LargeBinary)
)

data_product_metadata_table = Table(
    'data_product_metadata',
    metadata,
    Column('unique_id', String, primary_key=True),
    Column('title', String, nullable=False),
    Column('description', String),
    Column('keyword', ARRAY(String)),
    Column('publisher', String),
    Column('language', ARRAY(String)),
    Column('domain', ARRAY(String)),
    Column('license', String),
    Column('termsAndConditions', String),
    Column('policy', ARRAY(String)),
    Column('aggregation_of', ARRAY(String)),
    Column('distribution', JSONB),
    Column('Temporal_coverage', JSONB, nullable=True),
    Column('Spatial_coverage', String),
    Column('issued', DateTime, default=func.now(), nullable=False),
    Column('modified', ARRAY(DateTime), nullable=True), 
    Column('size', String), 
    Column('version', String), 
    Column('compliant_to', ARRAY(String)), 
    Column('relevant_to', ARRAY(String)), 
    Column('data_product_fields', JSONB), 
    Column('metadata_url', String),
    Column('support_tools_actions', String)
)   

# Create the table if it doesn't exist
metadata.create_all(engine)

df = None  # Initialize a variable to store the DataFrame
csv_loaded = False  # Flag to check if CSV is loaded
general_sensitive_columns = pd.DataFrame({
})  # Target sensitive columns
general_sensitive_columns_gr = pd.DataFrame({
})  # Add Greek columns if needed
lemmatizer = WordNetLemmatizer()


filtered_dfs = {}
merged_dataset_by_columns = None
last_merged_dataset = None

editable_products = {}  

app.config['MAX_CONTENT_LENGTH'] = 1000 * 1024 * 1024  # 1000 MB
#COMMENT Linux command "sudo nano /etc/nginx/nginx.conf", "sudo nginx -s reload"

@app.errorhandler(RequestEntityTooLarge)
def handle_large_request(error):
    return jsonify({'status': 'error', 'message': 'File is too large. Please upload a smaller file.'}), 413


#ALL ROUTES

#                                                           FOR VM
#@app.route('/product/swagger2.json')
#def swagger_json():
#    return send_file('/home/datamiteedc/Desktop/nikos tools/data product composition tool/static/swagger2.json')

@app.route('/product/upload_dataset', methods=['POST'])
def route_upload_dataset():
    global datasets, csv_loaded, uploaded_file_format, last_merged_dataset
    response, status_code, updated_last_merged_dataset = upload_dataset(datasets, csv_loaded, uploaded_file_format, last_merged_dataset)
    # Update last_merged_dataset
    last_merged_dataset = updated_last_merged_dataset
    
    return response, status_code

@app.route('/product/load_data_product', methods=['GET'])
def route_load_data_product():
    global datasets, editable_products
    return get_dataset(datasets, editable_products, factory, data_product_metadata_table, data_product_dataset_table)

@app.route('/product/retrieve_data_product', methods=['GET'])
def route_retrieve_data_product():
    global editable_products
    return retrieve_data_product(editable_products, factory, data_product_metadata_table, data_product_dataset_table)

def try_parse_json(field):
        try:
            return json.loads(field) if isinstance(field, str) else field
        except json.JSONDecodeError:
            return field  # Return as is if it's not valid JSON  

def exclude_powerfactory_files(datasets):
    """
    Excludes PowerFactory files or datasets with no DataFrame from the given datasets.
    
    :param datasets: List of dataset dictionaries
    :return: Filtered list of datasets
    """
    return [data for data in datasets if data.get('file_extension') != 'pfd' and data.get('dataframe') is not None]

@app.route('/product/analyze_datasets', methods=['POST'])
def analyze_datasets():
    """
    Route to analyze datasets.
    """
    
    global datasets, filtered_dfs, merged_dataset_by_columns  # Include other global variables
    datasets = exclude_powerfactory_files(datasets)
    return analyze_datasets_logic(datasets, filtered_dfs, merged_dataset_by_columns)

datasets_to_merge = []      


@app.route('/product/show_datasets', methods=['GET'])
def show_datasets():
    """
    API endpoint for showing imported datasets.

    Returns:
        JSON response with file names of imported datasets.
    """
    global datasets, filtered_dfs  # Access the global variables
    datasets = exclude_powerfactory_files(datasets)

    return show_datasets_logic(datasets, filtered_dfs)
    
@app.route('/product/merge_datasets_byPrimaryKey', methods=['POST'])
def merge_datasets_byPrimaryKey():
    """
    API endpoint for merging datasets by columns.

    Returns:
        JSON response indicating the status of the operation.
    """
    global datasets, last_merged_dataset, filtered_dfs, datasets_to_merge, merged_dataset_by_columns

    # Get JSON data from the request
    request_data = request.get_json()

    # Call the logic function from merge_datasets.py
    merged_df, response, status_code = merge_datasets_byPrimaryKey_logic(datasets, filtered_dfs, request_data, datasets_to_merge)

    if merged_df is not None:
        last_merged_dataset = merged_df  # Update the global last_merged_dataset
        merged_dataset_by_columns = merged_df 

    return response, status_code

@app.route('/product/download_merged_dataset_byPrimaryKey', methods=['GET'])
def download_merged_dataset_byPrimaryKey():
    """
    API endpoint for downloading merged dataset by columns.

    Returns:
        File download response.
    """
    global datasets, datasets_to_merge
    # Call the logic function from download_merged_dataset.py
    return download_merged_dataset_byPrimaryKey_logic(datasets, datasets_to_merge, enforce_integer_columns, request)
  
@app.route('/product/merge_datasets_bycolumns', methods=['POST'])
def merge_datasets_bycolumns():
    """
    API endpoint for merging datasets by columns.

    Returns:
        JSON response indicating the status of the operation.
    """
    global datasets, last_merged_dataset, filtered_dfs, datasets_to_merge, merged_dataset_by_columns

    # Get JSON data from the request
    request_data = request.get_json()

    # Call the logic function from merge_datasets.py
    merged_df, response, status_code = merge_datasets_bycolumns_logic(datasets, filtered_dfs, request_data, datasets_to_merge)

    if merged_df is not None:
        last_merged_dataset = merged_df  # Update the global last_merged_dataset
        merged_dataset_by_columns = merged_df 

    return response, status_code

@app.route('/product/download_merged_dataset_bycolumns', methods=['GET'])
def download_merged_dataset_bycolumns():
    """
    API endpoint for downloading merged dataset by columns.

    Returns:
        File download response.
    """
    global datasets, datasets_to_merge
    # Call the logic function from download_merged_dataset.py
    return download_merged_dataset_bycolumns_logic(datasets, datasets_to_merge, enforce_integer_columns, request)
    
   

    

    
@app.route('/product/merge_datasets_byrows', methods=['POST'])
def merge_datasets_byrows():
    global datasets, filtered_dfs, datasets_to_merge, merged_dataset_by_columns, last_merged_dataset  # Add merged_dataset_by_columns here

    # Get JSON data from the request
    request_data = request.get_json()

    # Call the logic function from merge_datasets.py
    merged_df, response, status_code = merge_datasets_byrows_logic(datasets, filtered_dfs, request_data, datasets_to_merge)

    if merged_df is not None:
        last_merged_dataset = merged_df  # Update the global last_merged_dataset
        merged_dataset_by_columns = merged_df 

    return response, status_code
    

@app.route('/product/download_merged_dataset_byrows', methods=['GET'])
def download_merged_dataset_byrows():

    """
    API endpoint for downloading merged dataset by rows.

    Returns:
        File download response.
    """
    global datasets, datasets_to_merge
    # Call the logic function from download_merged_dataset.py
    return download_merged_dataset_byrows_logic(datasets, datasets_to_merge, enforce_integer_columns, request)
    

@app.route('/product/get_all_datasets', methods=['GET'])
def get_all_datasets():
    """
    Get column names endpoint.

    Accepts GET requests to retrieve column names and file names of imported CSV files.
    """
    global datasets, filtered_dfs, merged_dataset_by_columns

    try:
        datasets = exclude_powerfactory_files(datasets)

        # Call the logic function to retrieve datasets and their columns
        csv_data = get_all_datasets_logic(datasets, filtered_dfs, merged_dataset_by_columns)

        # Return success response with the data
        return jsonify({'status': 'success', 'csv_data': csv_data}), 200

    except Exception as e:
        # Return error response if something goes wrong
        return jsonify({'status': 'error', 'message': str(e)}), 400

@app.route('/product/delete_columns', methods=['POST'])
def delete_columns():
    """
    Delete columns and rows endpoint.

    Accepts POST requests to delete specified columns or rows from imported CSV files.
    """
    global datasets, merged_dataset_by_columns, last_merged_dataset, filtered_dfs

    # Get JSON data from the request
    data = request.get_json()

    # Call the logic function from delete_columns_logic.py
    result, status_code, updated_last_merged_dataset, updated_merged_dataset  = delete_columns_logic(datasets, merged_dataset_by_columns, last_merged_dataset, filtered_dfs, data)

    # Update the global last_merged_dataset with the returned value
    last_merged_dataset = updated_last_merged_dataset
    merged_dataset_by_columns = updated_merged_dataset
    # Return the result and status code from the logic
    return jsonify(result), status_code


@app.route('/product/download_deleted_dataset', methods=['GET'])
def download_deleted_dataset():
    """
    Route to download a dataset in the specified format (CSV, Excel, or JSON).
    """
    global datasets, filtered_dfs, merged_dataset_by_columns
    
    # Get the file name and format from query parameters
    title = request.args.get('title')
    file_format = request.args.get('format', 'csv').lower()

    # Call the logic function from download_deleted_dataset_logic.py
    return download_deleted_dataset_logic(datasets, filtered_dfs, merged_dataset_by_columns, title, file_format, enforce_integer_columns)
    
def enforce_integer_columns(df):
    """Ensure that columns that should be integers remain as integers."""
    for col in df.select_dtypes(include=['float']):
        # Check if all non-NaN values in the column are integers
        if all(df[col].dropna().apply(float.is_integer)):
            # Convert to int but keep NaNs
            df[col] = df[col].astype('Int64')  # Use 'Int64' for nullable integers
    return df
    


@app.route('/product/retain_columns', methods=['POST'])
def retain_columns():
    """
    Retain columns and rows endpoint.

    Accepts POST requests to retain specified columns or rows from imported CSV files.
    """
    global datasets, merged_dataset_by_columns, last_merged_dataset, filtered_dfs

    # Get JSON data from the request
    data = request.get_json()

    # Call the logic function from delete_columns_logic.py
    result, status_code, updated_last_merged_dataset, updated_merged_dataset = retain_columns_logic(datasets, merged_dataset_by_columns, last_merged_dataset, filtered_dfs, data)

    # Update the global last_merged_dataset with the returned value
    last_merged_dataset = updated_last_merged_dataset
    merged_dataset_by_columns = updated_merged_dataset

    # Return the result and status code from the logic
    return jsonify(result), status_code


@app.route('/product/download_retained_dataset', methods=['GET'])
def download_retained_dataset():
    """
    Download retained dataset endpoint.

    Accepts GET requests to download a specified dataset in CSV, XLSX, or JSON format.
    """
    global datasets, filtered_dfs, merged_dataset_by_columns

    # Get the file name and format from query parameters
    title = request.args.get('title')
    format = request.args.get('format', 'csv').lower()

    # Call the download_dataset_logic function
    return download_dataset_logic(datasets, filtered_dfs, merged_dataset_by_columns, title, format, enforce_integer_columns)
    

# Custom JSON encoder to handle numpy data types when converting to JSON
class NpEncoder(json.JSONEncoder):
    def default(self, obj):
        # Check if the object is a numpy datetime or complex type and convert to string
        dtypes = (np.datetime64, np.complexfloating)
        if isinstance(obj, dtypes):
            return str(obj)
        # Convert numpy integer to int
        elif isinstance(obj, np.integer):
            return int(obj)
        # Convert numpy floating point to float
        elif isinstance(obj, np.floating):
            return float(obj)
        # Convert numpy array to a list
        elif isinstance(obj, np.ndarray):
            # If the array contains datetime or complex types, convert elements to string
            if any([np.issubdtype(obj.dtype, i) for i in dtypes]):
                return obj.astype(str).tolist()
            return obj.tolist()
        # Fallback to default JSON encoding for other types
        return super(NpEncoder, self).default(obj)    
    
@app.route('/product/get_datasets', methods=['GET'])
def get_datasets():
    global datasets, merged_dataset_by_columns
    try:
        datasets = exclude_powerfactory_files(datasets)

        # Call the logic function
        csv_data = get_datasets_logic(datasets, merged_dataset_by_columns)

        # Return the JSON response
        return jsonify({'status': 'success', 'csv_data': csv_data}), 200

    except Exception as e:
        return jsonify({'status': 'error', 'message': str(e)}), 400
    
@app.route('/product/filter_columns', methods=['POST'])
def filter_columns():
    global filtered_dfs, merged_dataset_by_columns, last_merged_dataset
    try:
        # Parse the request
        request_data = request.get_json()
        title = request_data.get('title')
        filter_criteria = request_data.get('filter_criteria')

        if not title or not filter_criteria:
            return jsonify({'status': 'error', 'message': 'File name and filter criteria must be provided.'})

        # Get the dataset
        if title == 'merged_dataset':
            df = merged_dataset_by_columns
        else:
            df_data = next((data for data in datasets if data['title'] == title), None)
            if df_data is None:
                return jsonify({'status': 'error', 'message': f'Dataset {title} not found.'})
            df = df_data['dataframe']

        def process_conditions(conditions, operator):
            """Process conditions within a single group."""
            group_mask = None
            for condition in conditions:
                column = condition['column']
                value = condition['value']
                condition_operator = condition.get('operator', '=')

                # Validate the operator
                if condition_operator not in ['=', '>=', '>', '<=', '<']:
                    raise ValueError(f"Invalid operator '{condition_operator}'. Allowed operators are '=', '>=', '>', '<=', '<'.")

                # Check column data type
                if not pd.api.types.is_numeric_dtype(df[column]):
                    if condition_operator != '=':
                        raise ValueError(f"Operator '{condition_operator}' is not allowed for non-numeric column '{column}'.")

                # Create the mask based on the operator
                if condition_operator == '=':
                    mask = df[column] == value
                elif condition_operator == '>':
                    mask = df[column] > value
                elif condition_operator == '>=':
                    mask = df[column] >= value
                elif condition_operator == '<':
                    mask = df[column] < value
                elif condition_operator == '<=':
                    mask = df[column] <= value
                else:
                    raise ValueError(f"Unsupported operator '{condition_operator}'.")

                # Combine conditions within the group
                if group_mask is None:
                    group_mask = mask
                else:
                    if operator == 'AND':
                        group_mask &= mask
                    elif operator == 'OR':
                        group_mask |= mask
                    else:
                        raise ValueError("Logical operator must be 'AND' or 'OR'.")

            return group_mask

        # Process the filter criteria groups
        combined_mask = None
        pending_masks = []  # To handle nested OR conditions

        for i, group in enumerate(filter_criteria):
            conditions = group.get('conditions', [])
            group_operator = group.get('logical_operator', 'AND').upper()

            # Process group conditions
            group_mask = process_conditions(conditions, group_operator)

            # Apply logical_operator_between_groups from the previous group
            if i == 0:
                combined_mask = group_mask
            else:
                inter_group_operator = filter_criteria[i - 1].get('logical_operator_between_groups', 'AND').upper()
                if inter_group_operator == 'AND':
                    combined_mask &= group_mask
                elif inter_group_operator == 'OR':
                    # Nest previous mask and push current mask for OR
                    pending_masks.append(combined_mask)
                    combined_mask = group_mask
                else:
                    raise ValueError("Logical operator must be 'AND' or 'OR'.")

        # Combine pending OR masks if any
        if pending_masks:
            for mask in pending_masks:
                combined_mask |= mask

        # Apply the final combined mask to the DataFrame
        filtered_df = df[combined_mask]

        # Drop rows where all values are NaN
        filtered_df.dropna(how='all', inplace=True)
        # Reset the index
        filtered_df.reset_index(drop=True, inplace=True)

        # Store the filtered DataFrame
        filtered_dfs[title] = filtered_df
        last_merged_dataset = filtered_df

        # Convert filtered DataFrame to JSON
        filtered_data = filtered_df.to_dict(orient='records')

        # Return the filtered data
        return jsonify({'status': 'success', 'filtered_data': filtered_data})
    except Exception as e:
        return jsonify({'status': 'error', 'message': f'Error: {str(e)}'})
    
@app.route('/product/download_filtered/<title>', methods=['GET'])
def download_filtered(title):
    global filtered_dfs
    try:
        # Get the desired file format from query parameters
        file_format = request.args.get('format', 'csv').lower()

        # Call the download logic
        response, error = download_filtered_logic(filtered_dfs, title, file_format)

        if error:
            return jsonify({'status': 'error', 'message': error}), 400

        return response

    except Exception as e:
        return jsonify({'status': 'error', 'message': f'Error: {str(e)}'}), 400
    
@app.route('/product/combine_files', methods=['POST'])
def combine_files():
    try:
        global last_merged_dataset
        # Extract selected files and output file name from the request body
        selected_files = request.json.get('selected_files', [])
        output_file_name = request.json.get('output_file_name', None)
        
        # Check if output file name is provided
        if not output_file_name:
            return jsonify({'status': 'error', 'message': 'Output file name is required'}), 400
        
        # Ensure a valid list of selected files is provided
        if not selected_files:
            return jsonify({'status': 'error', 'message': 'No files selected for combining'}), 400
        
        # Validate the output file name (ensure it ends with .zip)
        if not output_file_name.endswith('.zip'):
            return jsonify({'status': 'error', 'message': 'Output file name must end with ".zip"'}), 400
        
        # Initialize a zip file in memory
        zip_buffer = BytesIO()
        with zipfile.ZipFile(zip_buffer, 'w', zipfile.ZIP_DEFLATED) as zip_file:
            for file_name in selected_files:
                # Find the corresponding file in the datasets array
                matching_files = [data for data in datasets if data['title'] == file_name]
                logging.info(f"Selected files: {selected_files}")
                logging.info(f"Datasets: {[data['title'] for data in datasets]}")
                
                if not matching_files:
                    return jsonify({'status': 'error', 'message': f'File "{file_name}" not found in uploaded datasets'}), 400
                
                file_data = matching_files[0]  # Get the first match
                file_extension = file_data.get('file_extension', None)
                # Handle content for different file types
                if file_extension == 'pfd':  # For PowerFactory (PDF)
                    if not file_data['content']:
                        return jsonify({'status': 'error', 'message': f'Error: File "{file_name}" has no content to include'}), 400
                    zip_file.writestr(file_name, file_data['content'])
                
                elif file_data['dataframe'] is not None:  # For CSV, Excel, JSON
                    output_buffer = BytesIO()
                    if file_extension == 'csv':
                        file_data['dataframe'].to_csv(output_buffer, index=False)
                    elif file_extension == 'xlsx':
                        file_data['dataframe'].to_excel(output_buffer, index=False)
                    elif file_extension == 'json':
                        file_data['dataframe'].to_json(output_buffer, orient='records')
                    
                    # Write to the zip file
                    output_buffer.seek(0)
                    zip_file.writestr(file_name, output_buffer.read())
                else:
                    return jsonify({'status': 'error', 'message': f'Error: File "{file_name}" has no content to include'}), 400
                

        
        # Prepare the zip file for download
        zip_buffer.seek(0)
        last_merged_dataset = zip_buffer.getvalue()
        return send_file(
            zip_buffer,
            mimetype='application/zip',
            as_attachment=True,
            download_name=output_file_name  # Use the provided output file name
        )
    except Exception as e:
        return jsonify({'status': 'error', 'message': f'Error: {str(e)}'}), 500
    
@app.route('/product/download_data_product', methods=['GET'])
def download_data_product():
    """
    Route to download a dataset in the specified format (CSV, Excel, or JSON).
    """
    global datasets, filtered_dfs, merged_dataset_by_columns
    
    # Get the file name and format from query parameters
    title = request.args.get('title')
    file_format = request.args.get('format', 'csv').lower()

    # Call the logic function from download_deleted_dataset_logic.py
    return download_data_product_logic(datasets, filtered_dfs, merged_dataset_by_columns, title, file_format, enforce_integer_columns)  

#ALL ABOUT DATABASE SEARCH, UPDATE, SAVE, DELETE

    
@app.route('/product/save_data_product', methods=['POST'])
def save_data_product():
    try:
        global last_merged_dataset, datasets_to_merge

        if last_merged_dataset is None:
            return jsonify({'status': 'error', 'message': 'No merged dataset available to save'}), 400

        # Get the user input from the request
        data = request.json
        title = data.get('title')
        description = data.get('description')
        keyword = data.get('keyword', [])
        language = data.get('language', [])
        domain = data.get('domain', [])
        license = data.get('license', {})
        termsAndConditions = data.get('termsAndConditions', None)
        policy = data.get('policy', [])
        Temporal_coverage = data.get('Temporal_coverage', None)
        if isinstance(Temporal_coverage, str):
            Temporal_coverage = json.loads(Temporal_coverage)
        Spatial_coverage = data.get('Spatial_coverage', None)
        relevant_to = data.get('relevant_to', [])

        if not title:
            return jsonify({'status': 'error', 'message': 'Title is required'}), 400

        if not description:
            return jsonify({'status': 'error', 'message': 'Description is required'}), 400

        if not keyword or not isinstance(keyword, list) or len(keyword) == 0 or any(k == "" for k in keyword):
            return jsonify({'status': 'error', 'message': 'At least one valid keyword is required'}), 400

        if not domain or not isinstance(domain, list) or len(domain) == 0 or any(k == "" for k in domain):
            return jsonify({'status': 'error', 'message': 'Domain is required'}), 400


        
        # Generate the list of merged file titles
        merged_files_str = [ds['title'] for ds in datasets_to_merge]

        # Ensure termsAndConditions has non-empty values
        valid_terms_and_conditions = termsAndConditions and termsAndConditions.get('URL') and termsAndConditions.get('hash')
        compliant_to = ["GAIA-X", "EDC"] if title and description and keyword and domain and license and valid_terms_and_conditions and policy and language else None

        if compliant_to is None and title and description and keyword and language and policy:
            compliant_to = ["EDC"]
        if compliant_to is None and title and description and keyword and domain and license and valid_terms_and_conditions and policy:
            compliant_to = ["GAIA-X"]

        # Save metadata to the database first
        metadata_message, metadata_unique_id = save_metadata(
            None, title, description, keyword, language, domain, license, termsAndConditions, policy,
            Temporal_coverage, Spatial_coverage, relevant_to, None, None, merged_files_str, 0, compliant_to=compliant_to
        )

        # Check if metadata with the same values already exists
        if metadata_message.startswith("Metadata with the same values already exists"):
            return jsonify({'status': 'error', 'message': metadata_message}), 400

        # Save the last merged dataset to the database only if metadata creation succeeds
        save_message, unique_id, size = save_merged_dataset_to_db(last_merged_dataset)
        logging.info(save_message)
        response = {'status': 'success', 'message': save_message}

        if unique_id:
            # Generate the metadata and JSON URLs
            metadata_url = url_for('get_metadata', token=unique_id, _external=True)
            json_url = url_for('get_anonymized_data', token=unique_id, _external=True)

            # Update metadata with the dataset's unique ID and size
            metadata_message, metadata_unique_id = save_metadata(
                unique_id, title, description, keyword, language, domain, license, termsAndConditions, policy,
                Temporal_coverage, Spatial_coverage, relevant_to, metadata_url, json_url, merged_files_str, size, compliant_to=compliant_to
            )

            if metadata_unique_id is None:
                response.update({'status': 'error', 'message': metadata_message})
                return jsonify(response), 400

            # Update the response with metadata and dataset info
            response.update({
                'unique_id': unique_id,
                'json_url': json_url,
                'metadata_url': metadata_url,
                'metadata_message': metadata_message
            })

        return jsonify(response)

    except Exception as e:
        return jsonify({'status': 'error', 'message': str(e)}), 500
    
def extract_zip_contents(zip_data):
    """Extract the contents of a zip file as a dictionary of {file_name: file_content}."""
    try:
        zip_file = zipfile.ZipFile(BytesIO(zip_data))
        contents = {}
        for file_name in sorted(zip_file.namelist()):  # Sort to ensure consistent comparison order
            with zip_file.open(file_name) as file:
                contents[file_name] = file.read()  # Read the file's binary content
        return contents
    except Exception as e:
        raise ValueError(f"Error extracting zip contents: {str(e)}")    

def save_merged_dataset_to_db(merged_data):
    global data_product_dataset_table
    try:
        unique_id = str(uuid.uuid4())
        print(f"Received merged_data type: {type(merged_data)}")

        if isinstance(merged_data, pd.DataFrame):
            # Handle DataFrame (convert to JSON)
            merged_data_cleaned = merged_data.replace({pd.NA: None, np.nan: None})
            merged_data_dict = merged_data_cleaned.to_dict(orient='records')
            data_to_save = [{"unique_id": unique_id, "data": merged_data_dict}]  # Store JSON as a string
            size = sys.getsizeof(json.dumps(merged_data_dict))  # Size in terms of records

            # Check for existing dataset
            existing_dataset = session.query(data_product_dataset_table).filter(
                data_product_dataset_table.c.data == merged_data_dict
            ).first()
            if existing_dataset:
                message = f"Dataset with the same values already exists with unique ID: {existing_dataset.unique_id}"
                return message, existing_dataset.unique_id, size

        elif isinstance(merged_data, bytes):
            # Handle zip file (store as binary)
            new_zip_contents = extract_zip_contents(merged_data)
            data_to_save = [{"unique_id": unique_id, "zip_data": merged_data}]
            size = sys.getsizeof(merged_data)  # Size in bytes

            # Fetch all existing zip files from the database
            existing_datasets = session.query(
                data_product_dataset_table.c.unique_id, data_product_dataset_table.c.zip_data
            ).filter(
                data_product_dataset_table.c.zip_data.isnot(None)
            ).all()

            for existing_zip in existing_datasets:
                existing_zip_data = existing_zip.zip_data
                existing_zip_contents = extract_zip_contents(existing_zip_data)

                # Compare the contents of the existing zip with the new zip
                if new_zip_contents == existing_zip_contents:
                    message = f"Dataset with the same values already exists with unique ID: {existing_zip.unique_id}"
                    return message, existing_zip.unique_id, size
        else:
            raise ValueError("Unsupported merged data format")

        # Save the new dataset
        ins = data_product_dataset_table.insert().values(data_to_save)
        session.execute(ins)
        session.commit()

        return "Dataset saved successfully to the database", unique_id, size

    except SQLAlchemyError as e:
        session.rollback()  # Rollback the transaction on error
        return f"Database save error: {str(e)}", None, None
    except Exception as e:
        session.rollback()  # Rollback the transaction on error
        return f"Unexpected error: {str(e)}", None, None
 
@app.route('/product/update_data_product', methods=['PUT'])
def update_data_product():
    global editable_products, last_merged_dataset
    session = None
    try:
        # Get the request data (only accept unique_id, other fields are managed internally)
        data = request.json
        unique_id = data.get('unique_id')

        # Ensure the unique_id is provided
        if not unique_id:
            return jsonify({'status': 'error', 'message': 'Unique ID is required'}), 400

        # Check if the product was previously fetched and is editable
        if unique_id not in editable_products:
            return jsonify({'status': 'error', 'message': 'This data product is not editable or was not fetched before.'}), 400

        # Fetch the record from the database
        session = factory()
        metadata_table = data_product_metadata_table
        dataset_table = data_product_dataset_table

        # Find the existing metadata record
        existing_metadata = session.query(metadata_table).filter_by(unique_id=unique_id).first()
        if not existing_metadata:
            return jsonify({'status': 'error', 'message': 'Data product not found in the database'}), 404

        # Retrieve and update dataset in the data_product_dataset_table
        existing_dataset = session.query(dataset_table).filter_by(unique_id=unique_id).first()
        if not existing_dataset:
            return jsonify({'status': 'error', 'message': 'Dataset not found in the database'}), 404

        if last_merged_dataset is None:
            return jsonify({'status': 'error', 'message': 'There is no update on the data product. Please use at least one of the modules (aggregate by columns, aggregate by rows, delete/retain, filter)'}), 404

        # Check the type of `last_merged_dataset`
        if isinstance(last_merged_dataset, pd.DataFrame):
            # Handle DataFrame
            merged_data_cleaned = last_merged_dataset.replace({pd.NA: None, np.nan: None})
            merged_data_dict = merged_data_cleaned.to_dict(orient='records')
            size = sys.getsizeof(json.dumps(merged_data_dict))

            # Update the data field in the data_product_dataset_table
            session.query(dataset_table).filter_by(unique_id=unique_id).update({
                'data': merged_data_dict,
                'zip_data': None  # Clear any previous zip data if present
            })

            # Update data_product_fields in metadata
            updated_data_product_fields = [
                {'name': col, 'type': str(dtype)} for col, dtype in zip(last_merged_dataset.columns, last_merged_dataset.dtypes)
            ]
            session.query(metadata_table).filter_by(unique_id=unique_id).update({
                'data_product_fields': updated_data_product_fields,
                'size': size
            })

        elif isinstance(last_merged_dataset, bytes):
            # Handle bytes (zip file)
            new_zip_contents = extract_zip_contents(last_merged_dataset)
            size = sys.getsizeof(last_merged_dataset)

            # Fetch all existing zip files from the database
            #existing_datasets = session.query(
            #    dataset_table.c.unique_id, dataset_table.c.zip_data
            #).filter(
            #    dataset_table.c.zip_data.isnot(None)
            #).all()

            #for existing_zip in existing_datasets:
            #    existing_zip_data = existing_zip.zip_data
            #    existing_zip_contents = extract_zip_contents(existing_zip_data)

            #    # Compare the contents of the existing zip with the new zip
            #    if new_zip_contents == existing_zip_contents:
            #        return jsonify({
            #            'status': 'error',
            #            'message': f"Dataset with the same values already exists with unique ID: {existing_zip.unique_id}"
            #        }), 400
                
            # Update the zip_data field in the data_product_dataset_table
            session.query(dataset_table).filter_by(unique_id=unique_id).update({
                'zip_data': last_merged_dataset,
                'data': None  # Clear any previous JSON data if present
            })

        else:
            raise ValueError("Unsupported merged data format")

        # Update the aggregation_of field in metadata
        merged_files = [ds['title'] for ds in datasets_to_merge] if datasets_to_merge else None
        session.query(metadata_table).filter_by(unique_id=unique_id).update({
            'aggregation_of': merged_files
        })

        # Retrieve the current "modified" array and append the new timestamp
        current_modifications = existing_metadata.modified or []
        current_time = datetime.now()
        updated_modifications = current_modifications + [current_time]

        # Increment version
        current_version = existing_metadata.version or '1'
        new_version = str(int(current_version) + 1)

        # Update metadata with new modified array and version
        session.query(metadata_table).filter_by(unique_id=unique_id).update({
            'modified': cast(updated_modifications, ARRAY(DateTime)),
            'version': new_version
        })

        # Commit the changes to the database
        session.commit()

        return jsonify({'status': 'success', 'message': 'Data product updated successfully'}), 200

    except SQLAlchemyError as e:
        if session:
            session.rollback()
        return jsonify({'status': 'error', 'message': str(e)}), 500
    except Exception as e:
        if session:
            session.rollback()
        return jsonify({'status': 'error', 'message': str(e)}), 500
    finally:
        if session:
            session.close()

def save_metadata(unique_id, title, description, keyword, language, domain, license, termsAndConditions, policy, Temporal_coverage, Spatial_coverage, relevant_to, metadata_url, json_url, merged_files_str, size, compliant_to=None):
    global data_product_metadata_table, last_merged_dataset
    try:
        # Check if metadata with the same values already exists
        existing_metadata = session.query(data_product_metadata_table).filter_by(
            title=title,
            description=description,
            aggregation_of=merged_files_str
        ).first()

        if existing_metadata:
            # Return message and existing unique_id
            return f"Metadata with the same values already exists with unique ID: {existing_metadata.unique_id}", existing_metadata.unique_id

        publisher = "DATAMITE"

        # Prepare the license field with nested values
        license_data = {
            'title': license.get('title'),  # Optional
            'description': license.get('description'),  # Optional
            'accessURL': license.get('accessURL')  # Mandatory
        }

        # Prepare the distribution field with nested values
        distribution = [
            {
                'accessURL': json_url,
                'format': 'application/json'
            }
        ]

        # Prepare the termsAndConditions field
        termsAndConditions = {
            'URL': termsAndConditions.get('URL'),  
            'hash': termsAndConditions.get('hash'),  
        }

        # Prepare and validate the Temporal_coverage field
        try:
            start_date = datetime.fromisoformat(Temporal_coverage.get('startDate')).date() if Temporal_coverage.get('startDate') else None
            end_date = datetime.fromisoformat(Temporal_coverage.get('endDate')).date() if Temporal_coverage.get('endDate') else None
            Temporal_coverage = {
                'startDate': start_date.isoformat() if start_date else None,
                'endDate': end_date.isoformat() if end_date else None,
            }
        except ValueError as e:
            logging.error(f"Invalid date format in Temporal_coverage: {str(e)}")
            return f"Invalid date format in Temporal_coverage: {str(e)}", None

        # Prepare the Spatial_coverage field
        Spatial_coverage = {
            'location': Spatial_coverage.get('location'),  # Optional
            'lat': Spatial_coverage.get('lat'),  # Optional
            'long': Spatial_coverage.get('long')  # Mandatory
        }

        # Extract column names and data types from the last_merged_dataset
        #if last_merged_dataset is not None:
        #    data_product_fields = [
        #        {'name': col, 'type': str(dtype)} for col, dtype in zip(last_merged_dataset.columns, last_merged_dataset.dtypes)
        #    ]
        #else:
        #    data_product_fields = []
        # Extract column names and data types from the last_merged_dataset
        if isinstance(last_merged_dataset, pd.DataFrame):
            # If last_merged_dataset is a DataFrame, extract column information
            data_product_fields = [
                {'name': col, 'type': str(dtype)} for col, dtype in zip(last_merged_dataset.columns, last_merged_dataset.dtypes)
            ]
        else:
            # If last_merged_dataset is not a DataFrame (e.g., binary data), set an empty list
            data_product_fields = []

        logging.info(f"Keyword Type: {type(keyword)} - Value: {keyword}")

        # Prepare the metadata to be saved
        metadata_to_save = {
            'unique_id': unique_id,
            'title': title,
            'description': description,
            'keyword': keyword,
            'publisher': publisher,
            'language': language,
            'domain': domain,
            'license': json.dumps(license_data),
            'termsAndConditions': json.dumps(termsAndConditions),
            'policy': policy,
            'aggregation_of': merged_files_str,
            'distribution': distribution,
            'Temporal_coverage': Temporal_coverage,
            'Spatial_coverage': json.dumps(Spatial_coverage),
            'issued': func.now(),
            'modified': None,
            'size': size,
            'version': '1', 
            'compliant_to': compliant_to, 
            'relevant_to': relevant_to,
            'data_product_fields': data_product_fields,  # Add data product fields
            'metadata_url': metadata_url,
        }

        # Insert the metadata into the table
        ins = data_product_metadata_table.insert().values(metadata_to_save)
        session.execute(ins)
        session.commit()

        logging.info(f"Metadata for {title} saved successfully.")
        # Return success message and the unique_id
        return "Metadata saved successfully", unique_id
    
    except SQLAlchemyError as e:
        session.rollback()  # Rollback the transaction on error
        logging.error(f"Database save error (metadata): {str(e)}")
        # Return error message and None for unique_id
        return f"Database save error: {str(e)}", None
    
    except Exception as e:
        session.rollback()  # Rollback the transaction on error
        logging.error(f"Unexpected error (metadata): {str(e)}")
        # Return error message and None for unique_id
        return f"Unexpected error: {str(e)}", None

# Define the table structure for data_product_metadata
from sqlalchemy import Table, Column, String, MetaData

metadata = MetaData()

@app.route('/product/delete_data_products', methods=['DELETE'])
def delete_data_products():
    try:
        # Get the list of unique_ids from the request
        data = request.json
        unique_ids_str = data.get('unique_ids')

        # Check if unique_ids_str is missing
        if not unique_ids_str:
            return jsonify({'status': 'error', 'message': 'unique_ids are required'}), 400
        
        # Split the comma-separated IDs and strip any whitespace
        unique_ids = [uid.strip() for uid in unique_ids_str.split(',') if uid.strip()]

        # Pass the unique_ids to the logic function for deletion
        status, message = delete_data_products_logic(unique_ids)
        
        # Return appropriate response
        if status == 'success':
            return jsonify({'status': 'success', 'message': message}), 200
        else:
            return jsonify({'status': 'error', 'message': message}), 500

    except Exception as e:
        return jsonify({'status': 'error', 'message': str(e)}), 500


def delete_data_products_logic(unique_ids):
    global data_product_metadata_table, data_product_dataset_table, session
    try:
        # Begin transaction
        with session.begin():
            # Fetch the records that exist in the dataset table with the provided unique_ids
            existing_ids = session.query(data_product_dataset_table.c.unique_id).filter(
                data_product_dataset_table.c.unique_id.in_(unique_ids)
            ).all()
            
            # Convert the fetched tuples to a list of unique IDs
            existing_ids = [eid[0] for eid in existing_ids]

            # Find the IDs that are missing (not in the database)
            missing_ids = set(unique_ids) - set(existing_ids)
            found_ids = set(unique_ids) & set(existing_ids)

            if found_ids:
                # Proceed to delete records from both tables if there are valid IDs to delete
                session.query(data_product_dataset_table).filter(
                    data_product_dataset_table.c.unique_id.in_(found_ids)
                ).delete(synchronize_session=False)

                session.query(data_product_metadata_table).filter(
                    data_product_metadata_table.c.unique_id.in_(found_ids)
                ).delete(synchronize_session=False)

            # Commit the transaction
            session.commit()

            # Prepare the message based on found and missing IDs
            success_message = f"Deleted data products with IDs: {', '.join(found_ids)}" if found_ids else ""
            missing_message = f"The following unique_ids do not exist in the database: {', '.join(missing_ids)}" if missing_ids else ""

            # Return both success and missing messages
            return 'success', f"{success_message}. {missing_message}".strip()

    except SQLAlchemyError as e:
        session.rollback()  # Rollback in case of error
        return 'error', f"Database error: {str(e)}"
    
    except Exception as e:
        return 'error', f"Unexpected error: {str(e)}"

@app.route('/product/download/<dataset_id>', methods=['GET'])
def download_anonymized_data(dataset_id):
    global anonymized_dataset_table, engine

    try:
        # Fetch the dataset from the database using the unique ID
        dataset = session.query(data_product_dataset_table).filter_by(unique_id=dataset_id).first()

        if not dataset:
            return jsonify({'status': 'error', 'message': 'Dataset not found for the given ID'}), 404

        # Get the data from the dataset (assuming it's stored as JSONB in the database)
        data_product_data = dataset.data  # JSON object

        # Get the requested file format from query parameters (default is CSV)
        file_format = request.args.get('format', 'csv').lower()

        if file_format not in ['csv', 'json', 'xlsx']:
            return jsonify({'status': 'error', 'message': 'Invalid format. Supported formats are csv, json, xlsx.'}), 400

        # Convert the data into a DataFrame
        df = pd.DataFrame.from_records(data_product_data)

        # Generate a file response based on the format
        output = BytesIO()
        if file_format == 'csv':
            df.to_csv(output, index=False)
            output.seek(0)
            return send_file(
                output,
                mimetype='text/csv',
                as_attachment=True,
                download_name="data_product.csv"
            )
        elif file_format == 'json':
            output.write(df.to_json(orient='records').encode('utf-8'))
            output.seek(0)
            return send_file(
                output,
                mimetype='application/json',
                as_attachment=True,
                download_name="data_product.json"
            )
        elif file_format == 'xlsx':
            with pd.ExcelWriter(output, engine='xlsxwriter') as writer:
                df.to_excel(writer, index=False, sheet_name='Data Product')
            output.seek(0)
            return send_file(
                output,
                mimetype='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
                as_attachment=True,
                download_name="data_product.xlsx"
            )
    except Exception as e:
        logging.error(f"Error in download_data_product: {str(e)}")
        return jsonify({'status': 'error', 'message': f'Error in download_data_product: {str(e)}'}), 500          

@app.route('/product/update_metadata', methods=['PUT'])
def update_metadata():
    session = None  # Initialize the session variable

    try:
        # Get the request data
        data = request.json
        unique_id = data.get('unique_id')
        title = data.get('title')
        description = data.get('description')
        keyword = data.get('keyword')
        domain = data.get('domain')
        temporal_coverage = data.get('Temporal_coverage')
        spatial_coverage = data.get('Spatial_coverage')
        relevant_to = data.get('relevant_to')

        # Ensure unique_id is provided
        if not unique_id:
            return jsonify({'status': 'error', 'message': 'Unique ID is required'}), 400
        
        if not title:
            return jsonify({'status': 'error', 'message': 'Title is required'}), 400

        if not description:
            return jsonify({'status': 'error', 'message': 'Description is required'}), 400

        if not keyword:
            return jsonify({'status': 'error', 'message': 'Keyword is required'}), 400
        


        # Fetch the record from the database
        session = factory()
        metadata_table = data_product_metadata_table
        existing_metadata = session.query(metadata_table).filter_by(unique_id=unique_id).first()

        if not existing_metadata:
            return jsonify({'status': 'error', 'message': 'Metadata not found in the database'}), 404

        # Get the fields from the request and update only those that are provided
        fields_to_update = {}

             # Update description
        if description:
            fields_to_update['description'] = description.strip()

        # Update keyword
        if keyword:
            fields_to_update['keyword'] = keyword if isinstance(keyword, list) else [keyword.strip()]

        # Update domain
        if domain:
            fields_to_update['domain'] = domain if isinstance(domain, list) else [domain.strip()]

        # Update Temporal_coverage
        if temporal_coverage:
            try:
                start_date = datetime.fromisoformat(temporal_coverage.get('startDate')).date() if temporal_coverage.get('startDate') else None
                end_date = datetime.fromisoformat(temporal_coverage.get('endDate')).date() if temporal_coverage.get('endDate') else None
                fields_to_update['Temporal_coverage'] = ({
                    'startDate': start_date.isoformat() if start_date else None,
                    'endDate': end_date.isoformat() if end_date else None,
                })
            except ValueError as e:
                return jsonify({'status': 'error', 'message': f"Invalid date format in Temporal_coverage: {str(e)}"}), 400

        # Update Spatial_coverage
        if spatial_coverage:
            spatial_json = {
                'location': spatial_coverage.get('location', '').strip(),
                'lat': spatial_coverage.get('lat', '').strip(),
                'long': spatial_coverage.get('long', '').strip()
            }
            fields_to_update['Spatial_coverage'] = json.dumps(spatial_json)

        # Update relevant_to
        if relevant_to:
            if isinstance(relevant_to, list) and relevant_to:
                fields_to_update['relevant_to'] = relevant_to
            elif relevant_to == "":
                fields_to_update['relevant_to'] = None  # Set to null if empty

        # Check and update termsAndConditions field
        terms_and_conditions = data.get('termsAndConditions')
        existing_terms = json.loads(existing_metadata.termsAndConditions) if existing_metadata.termsAndConditions else {}

        url = terms_and_conditions.get('URL', '').strip() if terms_and_conditions else ''
        hash_value = terms_and_conditions.get('hash', '').strip() if terms_and_conditions else ''

        if url or hash_value:
            fields_to_update['termsAndConditions'] = json.dumps({'URL': url or "", 'hash': hash_value or ""})
        elif terms_and_conditions == "":
            fields_to_update['termsAndConditions'] = ""  # Store an empty string
        else:
            fields_to_update['termsAndConditions'] = json.dumps({'URL': "", 'hash': ""})  # Store empty JSON object

        # Check and update license field
        license_data = data.get('license')
        if license_data:
            license_json = {
                'title': license_data.get('title', '').strip(),
                'description': license_data.get('description', '').strip(),
                'accessURL': license_data.get('accessURL', '').strip()
            }
            fields_to_update['license'] = json.dumps(license_json)
        else:
            fields_to_update['license'] = json.dumps({'title': "", 'description': "", 'accessURL': ""})  # Empty license

        policy_data = data.get('policy')

        # If policy is provided as a string, strip any leading/trailing whitespace
        if isinstance(policy_data, str):
            policy_data = policy_data.strip()  # Remove leading/trailing spaces
            if policy_data == "":  # Explicitly check for an empty string
                policy_data = []  # Keep empty string as-is
        elif isinstance(policy_data, list):
            if not policy_data or all(item == "" for item in policy_data): 
                policy_data = []
        else:
            policy_data = []  # Default to empty string for other invalid cases

        # Update the fields to update
        fields_to_update['policy'] = policy_data

        language_data = data.get('language')

        # If policy is provided as a string, strip any leading/trailing whitespace
        if isinstance(language_data, str):
            language_data = language_data.strip()  # Remove leading/trailing spaces
            if language_data == "":  # Explicitly check for an empty string
                language_data = []  # Keep empty string as-is
        elif isinstance(language_data, list):
            if not language_data or all(item == "" for item in language_data): 
                language_data = []
        else:
            language_data = []  # Default to empty string for other invalid cases

        # Update the fields to update
        fields_to_update['language'] = language_data                  

        # Determine if "GAIA-X" should be added or removed based on termsAndConditions and license
        compliant_to_list = existing_metadata.compliant_to or []

        # Check if both termsAndConditions and license have meaningful data
        has_valid_terms = terms_and_conditions and (url and hash_value)
        has_valid_license = license_data and (license_data.get('accessURL') or license_data.get('title'))
        has_valid_policy = isinstance(policy_data, list) and bool(policy_data)
        has_valid_language = isinstance(language_data, list) and bool(language_data)

        if has_valid_terms and has_valid_license and has_valid_policy:
            if 'GAIA-X' not in compliant_to_list:
                compliant_to_list.append('GAIA-X')
        else:  # Remove "GAIA-X" if either is invalid or empty
            if 'GAIA-X' in compliant_to_list:
                compliant_to_list.remove('GAIA-X')

        if has_valid_language and has_valid_policy:
            if 'EDC' not in compliant_to_list:
                compliant_to_list.append('EDC')
        else:  # Remove "GAIA-X" if either is invalid or empty
            if 'EDC' in compliant_to_list:
                compliant_to_list.remove('EDC')                

        fields_to_update['compliant_to'] = compliant_to_list

        # If no fields are provided for update, return an error
        if not fields_to_update:
            return jsonify({'status': 'error', 'message': 'No fields to update were provided'}), 400

        # Update the fields in the database
        session.query(metadata_table).filter_by(unique_id=unique_id).update(fields_to_update)
        session.commit()

        return jsonify({'status': 'success', 'message': 'Metadata updated successfully'}), 200

    except SQLAlchemyError as e:
        session.rollback()
        return jsonify({'status': 'error', 'message': str(e)}), 500
    except Exception as e:
        if session:
            session.rollback()
        return jsonify({'status': 'error', 'message': str(e)}), 500
    finally:
        if session:
            session.close()
#data_product_metadata_table = Table(
#    'data_product_metadata',
#    metadata,
#    Column('unique_id', String, primary_key=True),
#    Column('file_name', String, nullable=False),
#    Column('description', String),
#    Column('keywords', String)
#)        
@app.route('/product/metadata/<token>', methods=['GET'])
def get_metadata(token):
    global data_product_metadata_table
    try:
        # Retrieve the metadata from the database
        result = session.query(data_product_metadata_table).filter_by(unique_id=token).first()

        if result:
            metadata = {
                'unique_id': result.unique_id,
                'title': result.title,
                'description': result.description,
                'keyword': result.keyword,
                'language': result.language,
                'publisher': result.publisher,
                'domain': result.domain,
                'license': json.loads(result.license) if result.license else None,
                'termsAndConditions': json.loads(result.termsAndConditions) if result.termsAndConditions else None,
                'policy': result.policy,
                'aggregation_of': result.aggregation_of,
                'distribution': result.distribution, 
                'Temporal_coverage': result.Temporal_coverage,
                'Spatial_coverage': json.loads(result.Spatial_coverage) if result.Spatial_coverage else None,
                'issued': result.issued,
                'modified': result.modified,
                'size': result.size,
                'version': result.version,
                'compliant_to': result.compliant_to,
                'relevant_to': result.relevant_to

            }
            return jsonify({'status': 'success', 'metadata': metadata})
        else:
            return jsonify({'status': 'error', 'message': 'Invalid token or metadata not found'}), 404
    except SQLAlchemyError as e:
        logging.error(f"Database error: {str(e)}")
        return jsonify({'status': 'error', 'message': f'Database error: {str(e)}'}), 500
    except Exception as e:
        logging.error(f"Error in get_metadata: {str(e)}")
        return jsonify({'status': 'error', 'message': f'Error in get_metadata_data: {str(e)}'}), 500

# Import the uploaded_datasets from file1.py
import upload_insert_datasets  

@app.route('/product/data_product/<token>', methods=['GET'])
def get_anonymized_data(token):
    global data_product_dataset_table
    try:
        # Retrieve the data from the database
        #query = anonymized_dataset_table.select().where(anonymized_dataset_table.c.unique_id == token)

        result=session.query(data_product_dataset_table).filter_by(unique_id=token).first()
        logging.info(f"Result: {result.unique_id}")
        #with engine.connect() as connection:
        #   result = connection.execute(query).fetchone()

        if result:
            json_data = result.data
            return jsonify({'status': 'success', 'data': json_data})
        else:
            return jsonify({'status': 'error', 'message': 'Invalid token or data not found'}), 404
    except SQLAlchemyError as e:
        logging.error(f"Database error: {str(e)}")
        return jsonify({'status': 'error', 'message': f'Database error: {str(e)}'}), 500
    except Exception as e:
        logging.error(f"Error in get_anonymized_data: {str(e)}")
        return jsonify({'status': 'error', 'message': f'Error in get_anonymized_data: {str(e)}'}), 500 

@app.route('/product/get_all_metadata', methods=['GET'])
def get_all_metadata():
    session = None
    try:
        # Create a session to interact with the database
        session = factory()

        # Get the number of records from the query parameter (with a default of 20, and a maximum of 100)
        num_records = request.args.get('limit', default=20, type=int)
        num_records = min(num_records, 100)  # Limiting the max number of records to 100

        # Fetch metadata records ordered by 'issued' date
        results = session.query(data_product_metadata_table).order_by(data_product_metadata_table.c.issued.desc()).limit(num_records).all()

        # Transform the results into a list of dictionaries for JSON serialization
        metadata_list = []
        for result in results:


            # Deserialize all relevant fields
            license_data = try_parse_json(result.license)
            spatial_coverage_data = try_parse_json(result.Spatial_coverage)
            temporal_coverage_data = try_parse_json(result.Temporal_coverage)
            data_product_fields_data = try_parse_json(result.data_product_fields)
            distribution_data = try_parse_json(result.distribution)
            terms_and_conditions_data = try_parse_json(result.termsAndConditions)            
            metadata_dict = {
                'unique_id': result.unique_id,
                'title': result.title,
                'description': result.description,
                'keyword': result.keyword,
                'publisher': result.publisher,
                'language': result.language,
                'domain': result.domain,
                'license': license_data,
                'termsAndConditions': terms_and_conditions_data,
                'policy': result.policy,
                'aggregation_of': result.aggregation_of,
                'distribution': distribution_data,
                'Temporal_coverage': temporal_coverage_data,
                'Spatial_coverage': spatial_coverage_data,
                'issued': result.issued,
                'modified': result.modified,
                'size': result.size,
                'version': result.version,
                'compliant_to': result.compliant_to,
                'relevant_to': result.relevant_to,
                'data_product_fields': data_product_fields_data,
                'metadata_url': result.metadata_url,
                'support_tools_actions': result.support_tools_actions
            }
            metadata_list.append(metadata_dict)

        # Return the metadata as JSON
        return jsonify({'status': 'success', 'data': metadata_list}), 200

    except Exception as e:
        return jsonify({'status': 'error', 'message': str(e)}), 500

    finally:
        if session:
            session.close()   

@app.route('/product/search_metadata', methods=['GET'])
def search_metadata():
    global data_product_metadata_table
    try:
        # Get the user input from the request
        search_criteria = request.args
        
        # Build the query dynamically based on the provided search criteria
        query = session.query(data_product_metadata_table)
        
        # Handle multiple values for each field
        if 'unique_id' in search_criteria:
            unique_ids = search_criteria['unique_id'].split(',')
            if isinstance(unique_ids, list):
                query = query.filter(data_product_metadata_table.c.unique_id.in_(unique_ids))
            else:
                query = query.filter_by(unique_id=unique_ids)
        
        if 'title' in search_criteria:
            file_names = search_criteria['title'].split(',')
            if isinstance(file_names, list):
                query = query.filter(data_product_metadata_table.c.title.in_(file_names))
            else:
                query = query.filter_by(title=file_names)
        
        if 'description' in search_criteria:
            descriptions = search_criteria['description'].split(',')
            if isinstance(descriptions, list):
                query = query.filter(or_(*[data_product_metadata_table.c.description.ilike(f"%{desc}%") for desc in descriptions]))
            else:
                query = query.filter(data_product_metadata_table.c.description.ilike(f"%{descriptions}%"))

        if 'keyword' in search_criteria:
            keywords_list = [kw.strip() for kw in search_criteria['keyword'].split(',') if kw.strip()]
            if keywords_list:
                query = query.filter(or_(*[data_product_metadata_table.c.keyword.any(kw) for kw in keywords_list]))

        #if 'data_url' in search_criteria:  # Add this block
        #    data_urls = search_criteria['data_url'].split(',')
        #    if isinstance(data_urls, list):
        #        query = query.filter(data_product_metadata_table.c.data_url.in_(data_urls))
        #    else:
        #        query = query.filter_by(data_url=data_urls)                

        if 'metadata_url' in search_criteria:
            metadata_urls = search_criteria['metadata_url'].split(',')
            if isinstance(metadata_urls, list):
                query = query.filter(data_product_metadata_table.c.metadata_url.in_(metadata_urls))
            else:
                query = query.filter_by(metadata_url=metadata_urls)

        if 'aggregation_of' in search_criteria:
            merged_files = search_criteria['aggregation_of'].split(',')
            query = query.filter(or_(*[data_product_metadata_table.c.aggregation_of.ilike(f"%{mf}%") for mf in merged_files]))   

        # New license title search block
        if 'license_title' in search_criteria:
            license_titles = search_criteria['license_title'].split(',')
            license_filters = []
            for title in license_titles:
                # Add filters to search in the JSON-encoded 'license' field by title
                license_filters.append(data_product_metadata_table.c.license.ilike(f'%\"title\": \"{title}\"%'))
            query = query.filter(or_(*license_filters)) 

        # New: Language filtering
        if 'language' in search_criteria:
            language_list = search_criteria['language'].split(',')
            query = query.filter(or_(*[data_product_metadata_table.c.language.any(lg.strip()) for lg in language_list]))

        # New: Domain filtering
        if 'domain' in search_criteria:
            domain_list = search_criteria['domain'].split(',')
            query = query.filter(or_(*[data_product_metadata_table.c.domain.any(dm.strip()) for dm in domain_list])) 

        if 'compliant_to' in search_criteria:
            domain_list = search_criteria['compliant_to'].split(',')
            query = query.filter(or_(*[data_product_metadata_table.c.compliant_to.any(dm.strip()) for dm in domain_list]))                                               

        if 'issued' in search_criteria:
            date_created_list = search_criteria['issued'].split(',')
            date_filters = []

            try:
                for date_entry in date_created_list:
                    date_entry = date_entry.strip()
                    if ' - ' in date_entry:  # Check for range using " - "
                        range_parts = date_entry.split(' - ')
                        if len(range_parts) == 2:
                            start_date_str = range_parts[0].strip()
                            end_date_str = range_parts[1].strip()

                            # Parse the start and end dates
                            start_date = datetime.strptime(start_date_str, '%Y-%m-%d').date()
                            end_date = datetime.strptime(end_date_str, '%Y-%m-%d').date()

                            # Check that the start date is not after the end date
                            if start_date > end_date:
                                return jsonify({'status': 'error', 'message': f'Invalid date range: {date_entry}. Start date must be before or equal to the end date.'}), 400

                            # Add the date range filter, ensuring the boundary dates are included
                            date_filters.append(
                                and_(
                                    func.date(data_product_metadata_table.c.issued) >= start_date,
                                    func.date(data_product_metadata_table.c.issued) <= end_date
                                )
                            )
                        else:
                            return jsonify({'status': 'error', 'message': f'Invalid date range format: {date_entry}. Use YYYY-MM-DD - YYYY-MM-DD.'}), 400
                    else:
                        # Single date handling
                        search_date = datetime.strptime(date_entry, '%Y-%m-%d').date()
                        date_filters.append(func.date(data_product_metadata_table.c.issued) == search_date)

                if date_filters:
                    # Combine all filters with OR, as each filter represents a valid date or range
                    query = query.filter(or_(*date_filters))

            except ValueError as e:
                return jsonify({'status': 'error', 'message': f'Invalid date format. Use YYYY-MM-DD or YYYY-MM-DD - YYYY-MM-DD. Error: {str(e)}'}), 400
            
        # Set up logging
        logging.basicConfig(level=logging.DEBUG)

        # New: Temporal coverage filtering
        if 'startDate' in search_criteria or 'endDate' in search_criteria:
            temporal_filters = []

            try:
                if 'startDate' in search_criteria:
                    start_dates = search_criteria['startDate'].split(',')
                    for start_date in start_dates:
                        start_date_obj = datetime.strptime(start_date.strip(), '%Y-%m-%d').date()
                        logging.debug(f"Start date from search criteria: {start_date_obj}")

                        # Extract startDate from Temporal_coverage JSONB field and compare it directly in the query
                        temporal_start_date = func.jsonb_extract_path_text(data_product_metadata_table.c.Temporal_coverage, 'startDate')
                        logging.debug(f"Temporal start date extracted from DB: {temporal_start_date}")

                        # Add filter for startDate comparison in the query
                        temporal_filters.append(
                            func.to_date(temporal_start_date, 'YYYY-MM-DD') >= start_date_obj
                        )

                if 'endDate' in search_criteria:
                    end_dates = search_criteria['endDate'].split(',')
                    for end_date in end_dates:
                        end_date_obj = datetime.strptime(end_date.strip(), '%Y-%m-%d').date()
                        logging.debug(f"End date from search criteria: {end_date_obj}")

                        # Extract endDate from Temporal_coverage JSONB field and compare it directly in the query
                        temporal_end_date = func.jsonb_extract_path_text(data_product_metadata_table.c.Temporal_coverage, 'endDate')
                        logging.debug(f"Temporal end date extracted from DB: {temporal_end_date}")

                        # Add filter for endDate comparison in the query
                        temporal_filters.append(
                            func.to_date(temporal_end_date, 'YYYY-MM-DD') <= end_date_obj
                        )

                if temporal_filters:
                    query = query.filter(and_(*temporal_filters))

            except ValueError as e:
                logging.error(f"Invalid date format error: {str(e)}")
                return jsonify({'status': 'error', 'message': f'Invalid date format. Use YYYY-MM-DD. Error: {str(e)}'}), 400

        # Execute the query and fetch results
        results = query.all()

        # Check if results are returned
        if results:
            metadata_list = []
            for result in results:
                # Check if issued is a string or a datetime object
                if isinstance(result.issued, str):
                    issued_str = result.issued  # Keep it as is or convert if needed
                else:
                    issued_str = result.issued.strftime('%Y-%m-%d') if hasattr(result.issued, 'strftime') else str(result.issued)

                if result.Temporal_coverage:
                    # Directly use the already parsed dictionary (no need for json.loads)
                    Temporal_coverage = {
                        'startDate': result.Temporal_coverage.get('startDate'),
                        'endDate': result.Temporal_coverage.get('endDate')
                    }
                    logging.debug(f"Temporal coverage from DB: {Temporal_coverage}")
                else:
                    Temporal_coverage = {'startDate': None, 'endDate': None}

                metadata_list.append({
                    'unique_id': result.unique_id,
                    'title': result.title,
                    'description': result.description,
                    'keyword': result.keyword,
                    'metadata_url': result.metadata_url,
                    'aggregation_of': result.aggregation_of,
                    'issued': issued_str,
                    'license_title': json.loads(result.license).get('title'),
                    'language': result.language,  # Include language in the response
                    'domain': result.domain,  # Include domain in the response
                    'compliant_to': result.compliant_to,
                    'Temporal_coverage': Temporal_coverage
                })
            
            return jsonify({'status': 'success', 'results': metadata_list})

        else:
            logging.debug('No matching metadata found')
            return jsonify({'status': 'error', 'message': 'No matching metadata found'}), 404

    except SQLAlchemyError as e:
        session.rollback() 
        logging.error(f"Database error: {str(e)}")
        return jsonify({'status': 'error', 'message': f'Database error: {str(e)}'}), 500
    except Exception as e:
        session.rollback() 
        logging.error(f"Unexpected error: {str(e)}")
        return jsonify({'status': 'error', 'message': f'Unexpected error: {str(e)}'}), 500 



#RESET ROUTE            
    
@app.route('/product/reset_datasets', methods=['GET'])
def reset_datasets():
    """
    Reset datasets endpoint.

    Accepts GET requests to reset the datasets array.
    """
    global merged_dataset_by_columns
    merged_dataset_ref = [merged_dataset_by_columns] 
    try:
        # Call the implementation function, passing the global variables
        response, status_code = reset_datasets_logic(
            datasets, filtered_dfs, merged_dataset_ref, datasets_to_merge
        )

        merged_dataset_by_columns = merged_dataset_ref[0]

        return jsonify(response), status_code

    except Exception as e:
        return jsonify({'status': 'error', 'message': str(e)}), 500
    
@app.route('/product/clear_datasets_from_get_dataset', methods=['POST'])
def clear_datasets_from_get_dataset():
    global datasets  # Access the global datasets variable

    # Filter out datasets loaded by get_dataset
    datasets_to_clear = [dataset for dataset in datasets if dataset.get('loaded_by_get_dataset')]
    datasets = [dataset for dataset in datasets if not dataset.get('loaded_by_get_dataset')]

    if not datasets_to_clear:
        return jsonify({'message': 'No datasets loaded by get_dataset to clear.'}), 200

    cleared_titles = [dataset['title'] for dataset in datasets_to_clear]
    logging.info(f"Cleared datasets loaded by get_dataset: {', '.join(cleared_titles)}")

    return jsonify({
        'message': 'Datasets loaded through get_dataset have been cleared.',
        'cleared_datasets': cleared_titles
    }), 200  
    
# Define the base URL for forwarding requests
BASE_URL = "http://backend:49788"

# Utility function to forward requests
def forward_request(endpoint, method="GET", data=None):
    url = f"{BASE_URL}{endpoint}"
    headers = {"Content-Type": "application/json"}
    try:
        if method == "GET":
            response = requests.get(url, headers=headers, params=request.args)
        elif method == "POST":
            response = requests.post(url, headers=headers, json=data)
        elif method == "PUT":
            response = requests.put(url, headers=headers, json=data)
        elif method == "DELETE":
            response = requests.delete(url, headers=headers, json=data)
        else:
            return jsonify({"error": "Unsupported method"}), 405

        return jsonify(response.json()), response.status_code
    except requests.exceptions.RequestException as e:
        return jsonify({"error": str(e)}), 500

# Endpoint to get Trino settings
@app.route("/product/trinosettings/get", methods=["GET"])
def get_trino_settings():
    return forward_request("/trinosettings/get")

# Endpoint to save Trino settings
@app.route("/product/trinosettings/save", methods=["POST"])
def save_trino_settings():
    data = request.get_json()
    return forward_request("/trinosettings/save", method="POST", data=data)


# Endpoint to get all Trino catalogs
@app.route("/product/trinocatalog/getall", methods=["GET"])
def get_all_trino_catalogs():
    return forward_request("/trinocatalog/getall")

# Endpoint to get a Trino catalog by ID
@app.route("/product/trinocatalog/getById", methods=["GET"])
def get_trino_catalog_by_id():
    return forward_request("/trinocatalog/getById")

# Endpoint to create a Trino catalog
@app.route("/product/trinocatalog/create", methods=["POST"])
def create_trino_catalog():
    data = request.get_json()
    return forward_request("/trinocatalog/create", method="POST", data=data)

# Endpoint to update a Trino catalog
@app.route("/product/trinocatalog/update", methods=["PUT"])
def update_trino_catalog():
    data = request.get_json()
    return forward_request("/trinocatalog/update", method="PUT", data=data)

# Endpoint to delete a Trino catalog
@app.route("/product/trinocatalog/delete", methods=["DELETE"])
def delete_trino_catalog():
    data = request.get_json()
    return forward_request("/trinocatalog/delete", method="DELETE", data=data)   

@app.route("/product//trinoquery/getall", methods=["GET"])
def getall_trinoquery():
    return forward_request("/trinoquery/getall")

@app.route("/product/trinoquery/getById", methods=["GET"])
def getById_trinoquery():
    return forward_request("/trinoquery/getById") 

@app.route("/product/trinoquery/execute", methods=["GET"])
def execute_trinoquery():
    return forward_request("/trinoquery/execute") 

@app.route("/product/trinoquery/create", methods=["POST"])
def create_trinoquery():
    data = request.get_json()
    return forward_request("/trinoquery/create", method="POST", data=data) 

@app.route("/product/trinoquery/update", methods=["PUT"])
def update_trinoquery():
    data = request.get_json()
    return forward_request("/trinoquery/update", method="PUT", data=data) 

@app.route("/product/trinoquery/delete", methods=["DELETE"])
def delete_trinoquery():
    data = request.get_json()
    return forward_request("/trinoquery/delete", method="DELETE", data=data) 

@app.route("/product/trinoquery/getExecutedResults", methods=["GET"])
def getExecutedResults_trinoquery():
    return forward_request("/trinoquery/getExecutedResults") 

@app.route("/product/trinoquery/getExecutedResultOnCSV", methods=["GET"])
def getExecutedResultOnCSV_trinoquery():
    trino_service_url = "http://localhost:49788/trinoquery/getExecutedResultOnCSV"  # Change to actual backend URL

    # Forward the request with the same query parameters
    response = requests.get(trino_service_url, params=request.args, stream=True)

    if response.status_code == 200:
        return Response(
            response.iter_content(chunk_size=1024),
            content_type="text/csv",
            headers={
                "Content-Disposition": 'attachment; filename="output.csv"'
            },
            status=200
        )
    else:
        return Response(
            response.text,
            content_type="application/json",
            status=response.status_code
        )

@app.route("/product/trinoquery/instantExecute", methods=["POST"])
def instantExecute_trinoquery():
    data = request.get_json()
    return forward_request("/trinoquery/instantExecute", method="POST", data=data) 

@app.route("/product/trinoquery/deleteResult", methods=["DELETE"])
def deleteResult_trinoquery():
    data = request.get_json()
    return forward_request("/trinoquery/deleteResult", method="DELETE", data=data) 
  

SWAGGER_URL = '/product'
API_URL = '/static/swagger2.json'
swaggerui_blueprint = get_swaggerui_blueprint(
    SWAGGER_URL,
    API_URL,
    config={
        'app_name': "Data product composition"
    }
)
app.register_blueprint(swaggerui_blueprint, url_prefix=SWAGGER_URL)

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5001, debug=True)