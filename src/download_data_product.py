# Copyright (c) 2024 Centre for Research and Technology Hellas (CERTH) / Information Technologies Institute (ITI)
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
# 
# SPDX-License-Identifier: MIT
# 
# Contributors:
#     Nikolaos Tepelidis - Author
#     Vasileios Siopidis - Couthor
#     Georgios Nikolaidis - Couthor
#     Konstantinos Votis - Couthor



import io
from flask import jsonify, make_response
import pandas as pd

def download_data_product_logic(datasets, filtered_dfs, merged_dataset_by_columns, title, file_format, enforce_integer_columns):
    """
    Logic to download a dataset in the specified format (CSV, Excel, or JSON).

    Args:
        datasets: List of available datasets.
        filtered_dfs: Dictionary of filtered DataFrames.
        merged_dataset_by_columns: Merged dataset (if any).
        title: Title of the dataset to download.
        file_format: The format in which to download the dataset (CSV, Excel, JSON).

    Returns:
        A tuple containing:
        - A Flask response object with the dataset in the specified format.
        - A status code (200 for success, 400 or 500 for errors).
    """
    try:
        # Check if the title is provided
        if not title:
            return jsonify({'status': 'error', 'message': 'File name not specified'}), 400

        # Check if there are datasets available
        if not datasets and not filtered_dfs and merged_dataset_by_columns is None:
            return jsonify({'status': 'error', 'message': 'No datasets to download'}), 400

        # Remove existing file extensions from the file name
        file_name_without_extension = title.rsplit('.', 1)[0]

        # Find the specified dataset
        if title == 'merged_dataset':
            dataframe = merged_dataset_by_columns
        elif title.startswith('filtered_'):
            original_file_name = title.replace('filtered_', '')
            dataframe = filtered_dfs.get(original_file_name)
        else:
            dataset = next((d for d in datasets if d['title'] == title), None)
            dataframe = dataset['dataframe'] if dataset else None

        # Check if the dataset is found
        if dataframe is None:
            return jsonify({'status': 'error', 'message': f'Dataset {title} not found'}), 400

        # Ensure the DataFrame has integer columns enforced (Assume enforce_integer_columns is defined elsewhere)
        dataframe = enforce_integer_columns(dataframe)

        # Create file-like object
        output_buffer = io.BytesIO()

        # Generate the file in the specified format
        if file_format == 'csv':
            dataframe.to_csv(output_buffer, index=False, float_format='%.0f', sep=',')
            content_type = 'text/csv'
            file_extension = 'csv'
        elif file_format == 'xlsx':
            dataframe.to_excel(output_buffer, index=False)
            content_type = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
            file_extension = 'xlsx'
        elif file_format == 'json':
            output_buffer.write(dataframe.to_json(orient='records').encode())
            content_type = 'application/json'
            file_extension = 'json'
        else:
            return jsonify({'status': 'error', 'message': 'Unsupported file format'}), 400

        # Seek to the beginning of the buffer
        output_buffer.seek(0)

        # Create a response object
        response = make_response(output_buffer.getvalue())
        response.headers["Content-Disposition"] = f"attachment; filename=data_product.{file_extension}"
        response.headers["Content-type"] = content_type

        return response, 200

    except Exception as e:
        return jsonify({'status': 'error', 'message': str(e)}), 500