# Copyright (c) 2024 Centre for Research and Technology Hellas (CERTH) / Information Technologies Institute (ITI)
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
# 
# SPDX-License-Identifier: MIT
# 
# Contributors:
#     Nikolaos Tepelidis - Author
#     Vasileios Siopidis - Couthor
#     Georgios Nikolaidis - Couthor
#     Konstantinos Votis - Couthor
from io import BytesIO
import pandas as pd
import io
import json
import csv
from sqlalchemy.exc import SQLAlchemyError
from sqlalchemy import select, join, cast, String, or_
from flask import jsonify, request
from werkzeug.utils import secure_filename
import logging
import zipfile
# Other necessary imports based on your current project setup

# Define your helper functions

def try_parse_json(field):
        try:
            return json.loads(field) if isinstance(field, str) else field
        except json.JSONDecodeError:
            return field  # Return as is if it's not valid JSON  

def detect_csv_dialect(file):
    """Detect the CSV dialect (delimiter) using csv.Sniffer."""
    try:
        sample = file.read(2048).decode('utf-8')
        file.seek(0)
        dialect = csv.Sniffer().sniff(sample)
        return dialect
    except csv.Error:
        return None

def check_mixed_types(df):
    """Check for mixed data types in DataFrame columns."""
    for column in df.columns:
        try:
            pd.to_numeric(df[column], errors='raise')
        except ValueError:
            types = df[column].apply(type).unique()
            logging.debug(f"Column '{column}' types: {types}")
            if len(types) > 1:
                return column, types
    return None, None

# Other functions
uploaded_datasets = []  # This stores {'title': title, 'data': dataframe}
def upload_dataset(datasets, csv_loaded, uploaded_file_format, last_merged_dataset):
    try:
        global uploaded_datasets  # Ensure modifications affect the shared instance
        uploaded_file_names = []  # Initialize an empty list to store uploaded file names
        uploaded_dataframes = []  
        for file in request.files.getlist('files'):  # Iterate over uploaded files
            title = file.filename  # Get the filename of the uploaded file

            if title == '':  # Check if the file is empty
                return jsonify({'status': 'error', 'message': 'Error: File is empty.'}), 400, last_merged_dataset

            filename = secure_filename(title)  # Secure the filename to prevent directory traversal
            file_extension = filename.rsplit('.', 1)[-1].lower()  # Get the file extension

            if file_extension not in ['csv', 'xlsx', 'json', 'pfd']:  # Check if the file format is supported
                return jsonify({'status': 'error', 'message': 'Invalid file format'}), 400, last_merged_dataset  # Return error response

            if any(data['title'] == title for data in datasets):
                return jsonify({'status': 'error', 'message': f'Error: File {title} has already been uploaded.'}), 400, last_merged_dataset

            file.seek(0)  # Reset the file pointer to the beginning
            file_size_bytes = len(file.read())  # Calculate the file size in bytes
            file.seek(0)  # Reset the file pointer to the beginning again for reading into DataFrame
            size = round(file_size_bytes / 1024, 2)  # Convert file size to kilobytes

            if file_extension == 'pfd':
                # For PFD files, append metadata and file content to the datasets array
                file_content = file.read()  # Read the raw file content
                datasets.append({
                    'title': title,
                    'size': size,
                    'file_extension': file_extension,
                    'content': file_content,  # Store the raw file content
                    'dataframe': None
                })
                uploaded_file_names.append(title)
                continue  # Skip further processing for PFD files

            # Read the uploaded file based on its format
            if file_extension == 'csv':
                #df = pd.read_csv(file, dtype=str)
                dialect = detect_csv_dialect(file)
                # Convert each column to its appropriate type based on its contents
                if not dialect:
                    return jsonify({'status': 'error', 'message': 'Error: Unable to detect CSV dialect.'}), 400, last_merged_dataset
                file.seek(0)
                df = pd.read_csv(file, delimiter=',', dtype=str)  # Read all columns as strings to ensure consistency      
                        
                for column in df.columns:
                    logging.debug(f"Initial type of column '{column}': {df[column].dtype}")

                # Attempt to convert each column to its appropriate type
                for column in df.columns:
                    df[column] = df[column].apply(lambda x: pd.to_numeric(x, errors='ignore'))

                # Log the types after conversion
                for column in df.columns:
                    logging.debug(f"Type of column '{column}' after conversion: {df[column].dtype}")
                                      

                mixed_column, types = check_mixed_types(df)
                if mixed_column:
                    return jsonify({'status': 'error', 'message': f'Error: Column "{mixed_column}" contains mixed data types: {types}'}), 400, last_merged_dataset  
            elif file_extension == 'xlsx':
                df = pd.read_excel(file)
            elif file_extension == 'json':
                df = pd.read_json(file) 




            # Check for mixed data types in columns
            for column in df.columns:
                column_types = df[column].apply(type).unique()
                if len(column_types) > 1:
                    return jsonify({'status': 'error', 'message': f'Error: Column "{column}" contains mixed data types: {column_types}'}), 400, last_merged_dataset 
            # Update the csv_loaded and uploaded_file_format
            csv_loaded = True
            uploaded_file_format = file_extension

            datasets.append({
                'title': title,
                'size': size,
                'file_extension': file_extension,  # Add the file extension here
                'content': None,  # Set None for non-PFD files
                'dataframe': df
            })
            uploaded_file_names.append(title)
            uploaded_dataframes.append(df) 
            # Store dataframe in the global uploaded_datasets
            uploaded_datasets.append({'title': title, 'data': df})

        #        # Merge data from uploaded files into last_merged_dataset
        if uploaded_dataframes:
            #last_merged_dataset = pd.concat([last_merged_dataset] + uploaded_dataframes, ignore_index=True)    
            last_merged_dataset = uploaded_dataframes[-1] 

        # **Print debugging output**
        print("====== LAST MERGED DATASET ======")
        print(last_merged_dataset)  # Print raw DataFrame
        #print(last_merged_dataset.to_dict(orient='records'))  # Print as dictionary (useful for JSON response)
        print("=================================")            
        # Merge uploaded datasets into last_merged_dataset
        #if uploaded_datasets:
        #    last_merged_dataset = []  # Store separate DataFrames as a dictionary

        #    for dataset in uploaded_datasets:
        #        title = dataset['title']
        #        df = dataset['data']

                # Append dataset with title and data as a dictionary
        #        last_merged_dataset.append({
        #            'title': title,
        #            'data': df.to_dict(orient='records')  # Convert DataFrame to dictionary for structured storage
        #        })
            # Print structure of last_merged_dataset
        #    print("====== LAST MERGED DATASET CONTENTS ======")
        #    for entry in last_merged_dataset:
        #        print(f"File: {entry['title']}")
        #        print(pd.DataFrame(entry['data']))  # Convert back to DataFrame for display
        #        print("\n-----------------------------\n")
       

        return jsonify({
            'status': 'success',
            'message': f'Dataset ({", ".join(uploaded_file_names)}) uploaded successfully',
            'csv_loaded': csv_loaded,
            'uploaded_file_format': uploaded_file_format
        }), 200, last_merged_dataset
    except Exception as e:
        return jsonify({'status': 'error', 'message': f'Error: {str(e)}'}), 500, last_merged_dataset 

def get_data_by_unique_ids_or_file_names(factory, editable_products, data_product_metadata_table, data_product_dataset_table, unique_ids=None, file_names=None):
    try:
        session = factory()
        metadata_table = data_product_metadata_table
        dataset_table = data_product_dataset_table

        j = join(metadata_table, dataset_table, cast(metadata_table.c.unique_id, String) == cast(dataset_table.c.unique_id, String))

        query = select(metadata_table, *[column for column in dataset_table.c if column.name not in ['unique_id']]).select_from(j)

        conditions = []
        if unique_ids:
            conditions.append(metadata_table.c.unique_id.in_(unique_ids))
        if file_names:
            conditions.append(metadata_table.c.title.in_(file_names))

        if not conditions:
            return None

        query = query.where(or_(*conditions))

        results = session.execute(query).mappings().all()
        # Track the unique IDs of the fetched results
        if results:
            
            
            modified_results = []

            for result in results:
                editable_products[result['unique_id']] = result
                modified_result = dict(result)  # Create a mutable dictionary from the RowMapping
                #modified_result.pop('data', None)
                modified_result['license'] = try_parse_json(modified_result['license'])
                modified_result['Spatial_coverage'] = try_parse_json(modified_result.get('Spatial_coverage', ''))
                modified_result['Temporal_coverage'] = try_parse_json(modified_result.get('Temporal_coverage', ''))
                modified_result['data_product_fields'] = try_parse_json(modified_result.get('data_product_fields', ''))
                modified_result['distribution'] = try_parse_json(modified_result.get('distribution', ''))
                modified_result['termsAndConditions'] = try_parse_json(modified_result.get('termsAndConditions', ''))
    
                modified_results.append(modified_result)
            return modified_results    # Convert each result to a dictionary
        else:
            return None

    except SQLAlchemyError as e:
        return None
    finally:
        if session:
            session.close()

def calculate_csv_size(df):
    buffer = io.StringIO()
    df.to_csv(buffer, index=False)
    buffer.seek(0)
    csv_content = buffer.getvalue()
    file_size_bytes = len(csv_content.encode('utf-8'))
    size = round(file_size_bytes / 1024, 2)
    return size

def get_dataset(datasets, editable_products, factory, data_product_metadata_table, data_product_dataset_table):
    unique_ids = request.args.get('unique_id')
    file_names = request.args.get('title')

    unique_ids = unique_ids.split(',') if unique_ids else []
    file_names = file_names.split(',') if file_names else []

    existing_unique_ids = [dataset['unique_id'] for dataset in datasets if 'unique_id' in dataset]
    existing_file_names = [dataset['title'] for dataset in datasets if 'title' in dataset]

    # Initialize lists to hold ids and file names that are already loaded
    duplicate_ids = []
    duplicate_file_names = []

    # Separate the ids and file names that are already in the datasets list
    filtered_unique_ids = []
    for uid in unique_ids:
        if uid in existing_unique_ids:
            duplicate_ids.append(uid)
        else:
            filtered_unique_ids.append(uid)

    filtered_file_names = []
    for fname in file_names:
        if fname in existing_file_names:
            duplicate_file_names.append(fname)
        else:
            filtered_file_names.append(fname)

    # Create a message for duplicate ids and file names
    duplicate_messages = []
    if duplicate_ids:
        duplicate_messages.append(f"IDs {', '.join(duplicate_ids)} already exist in the tool.")
    if duplicate_file_names:
        duplicate_messages.append(f"File names {', '.join(duplicate_file_names)} already exist in the tool.")

    # If no new ids or file names are left to query, return the duplicate message
    if not filtered_unique_ids and not filtered_file_names:
        return jsonify({'error': 'No new datasets to load', 'message': ' '.join(duplicate_messages)}), 400

    # Query the database with the filtered ids and file names
    results = get_data_by_unique_ids_or_file_names(factory, editable_products, data_product_metadata_table, data_product_dataset_table, unique_ids=filtered_unique_ids, file_names=filtered_file_names)

    if results:
        modified_results = []  # Store modified results without 'data'
        
        for result in results:
            modified_result = dict(result)  # Create a mutable dictionary from the RowMapping
            # Keep 'data' in the result but don't return it
            data = modified_result.pop('data', None)
            zip_data = modified_result.pop('zip_data', None)

            
            modified_result['license'] = try_parse_json(modified_result['license'])
            modified_result['Spatial_coverage'] = try_parse_json(modified_result.get('Spatial_coverage', ''))
            modified_result['Temporal_coverage'] = try_parse_json(modified_result.get('Temporal_coverage', ''))
            modified_result['data_product_fields'] = try_parse_json(modified_result.get('data_product_fields', ''))
            modified_result['distribution'] = try_parse_json(modified_result.get('distribution', ''))
            modified_result['termsAndConditions'] = try_parse_json(modified_result.get('termsAndConditions', ''))

            modified_results.append(modified_result)
            # If 'data' is present, create a DataFrame and calculate its size
            if data:
                try:
                    df = pd.DataFrame(data)
                    size = calculate_csv_size(df)
                    
                    # Create a dataset entry with the size and append it to the datasets list
                    dataset_entry = {
                        'title': result['title'],
                        'unique_id': result['unique_id'],
                        'size': f"{size} KB",  # Add the size here
                        'dataframe': df,
                        'loaded_by_get_dataset': True
                    }
                    datasets.append(dataset_entry)
                    logging.info(f"Dataset {result['title']} added to the datasets list.")
                except ValueError as e:
                    logging.error(f"Failed to process JSON data for dataset {result['title']}: {str(e)}")

            if zip_data:
                try:
                    # Extract files if the data is binary (ZIP format)
                    extracted_files = extract_zip_contents(zip_data)
                    
                    for file_name, file_content in extracted_files.items():

                        if file_name.endswith('.pfd'):
                            # Append metadata and raw file content to the datasets array
                            file_size_kb = len(file_content) / 1024  # Calculate size in KB
                            datasets.append({
                                'title': file_name,
                                'unique_id': result['unique_id'],
                                'size': f"{file_size_kb:.2f} KB",
                                'file_extension': 'pfd',
                                'content': file_content,  # Store the raw file content
                                'dataframe': None,  # No DataFrame for .pfd files
                                'loaded_by_get_dataset': True
                            })
                            logging.info(f"PowerFactory file {file_name} added to the datasets list.")
                            continue  # Skip further processing for this file                   
                        # Process JSON files
                        if file_name.endswith('.json'):
                            df = pd.read_json(BytesIO(file_content))
                        # Process CSV files
                        elif file_name.endswith('.csv'):
                            df = pd.read_csv(BytesIO(file_content))
                        # Process XLSX files
                        elif file_name.endswith('.xlsx'):
                            df = pd.read_excel(BytesIO(file_content))
                        else:
                            logging.warning(f"Unsupported file format: {file_name}")
                            continue

                        # Calculate size and append to datasets
                        size = calculate_csv_size(df)
                        dataset_entry = {
                            'title': file_name,
                            'unique_id': result['unique_id'],
                            'size': f"{size} KB",
                            'dataframe': df,
                            'loaded_by_get_dataset': True
                        }
                        datasets.append(dataset_entry)
                        logging.info(f"File {file_name} from dataset {result['title']} added to the datasets list.")


                except ValueError as e:
                    logging.error(f"Failed to extract or process files from binary data: {str(e)}")
                    return jsonify({'error': f"Failed to process dataset {result['title']}: {str(e)}"}), 500
        # Log the contents of the datasets table
        logging.info("Contents of datasets:")
        for dataset in datasets:
            logging.info(f"Title: {dataset['title']}, Unique ID: {dataset.get('unique_id')}, Size: {dataset.get('size')}")
        # Return results without 'data'
        return jsonify({
            'results': modified_results,
            'message': ' '.join(duplicate_messages)
        }), 200
    else:
        return jsonify({
            'error': 'No datasets found',
            'message': ' '.join(duplicate_messages)
        }), 404
    
def extract_zip_contents(zip_data):
    """Extract the contents of a zip file as a dictionary of {file_name: file_content}."""
    try:
        zip_file = zipfile.ZipFile(BytesIO(zip_data))
        contents = {}
        for file_name in sorted(zip_file.namelist()):  # Sort to ensure consistent comparison order
            with zip_file.open(file_name) as file:
                contents[file_name] = file.read()  # Read the file's binary content
        return contents
    except Exception as e:
        raise ValueError(f"Error extracting zip contents: {str(e)}") 
    
def retrieve_data_product(editable_products, factory, data_product_metadata_table, data_product_dataset_table):
    unique_ids = request.args.get('unique_id')
    file_names = request.args.get('title')

    unique_ids = unique_ids.split(',') if unique_ids else []
    file_names = file_names.split(',') if file_names else []

    # Query the database with the provided ids and file names
    results = get_data_by_unique_ids_or_file_names(factory, editable_products, data_product_metadata_table, data_product_dataset_table, unique_ids=unique_ids, file_names=file_names)

    if results:
        modified_results = []  # Store modified results without 'data'

        for result in results:
            modified_result = dict(result)  # Create a mutable dictionary from the RowMapping
            # Keep 'data' in the result but don't return it
            data = modified_result.pop('data', None)
            zip_data = modified_result.pop('zip_data', None)

            modified_result['license'] = try_parse_json(modified_result['license'])
            modified_result['Spatial_coverage'] = try_parse_json(modified_result.get('Spatial_coverage', ''))
            modified_result['Temporal_coverage'] = try_parse_json(modified_result.get('Temporal_coverage', ''))
            modified_result['data_product_fields'] = try_parse_json(modified_result.get('data_product_fields', ''))
            modified_result['distribution'] = try_parse_json(modified_result.get('distribution', ''))
            modified_result['termsAndConditions'] = try_parse_json(modified_result.get('termsAndConditions', ''))

            modified_results.append(modified_result)

            # If 'data' is present, calculate its size without storing it
            if data:
                try:
                    df = pd.DataFrame(data)
                    size = calculate_csv_size(df)
                    logging.info(f"Calculated size for dataset {result['title']}: {size} KB")
                except ValueError as e:
                    logging.error(f"Failed to process JSON data for dataset {result['title']}: {str(e)}")

            # If ZIP data is present, extract and process files without storing
            if zip_data:
                try:
                    extracted_files = extract_zip_contents(zip_data)
                    for file_name, file_content in extracted_files.items():
                        if file_name.endswith('.pfd'):
                            file_size_kb = len(file_content) / 1024  # Calculate size in KB
                            logging.info(f"Processed PowerFactory file {file_name} with size {file_size_kb:.2f} KB")
                            continue

                        if file_name.endswith('.json'):
                            df = pd.read_json(BytesIO(file_content))
                        elif file_name.endswith('.csv'):
                            df = pd.read_csv(BytesIO(file_content))
                        elif file_name.endswith('.xlsx'):
                            df = pd.read_excel(BytesIO(file_content))
                        else:
                            logging.warning(f"Unsupported file format: {file_name}")
                            continue

                        size = calculate_csv_size(df)
                        logging.info(f"Processed file {file_name} with size {size} KB")

                except ValueError as e:
                    logging.error(f"Failed to extract or process files from binary data: {str(e)}")
                    return jsonify({'error': f"Failed to process dataset {result['title']}: {str(e)}"}), 500

        # Return results without 'data'
        return jsonify({'results': modified_results}), 200
    else:
        return jsonify({'error': 'No datasets found'}), 404    
    