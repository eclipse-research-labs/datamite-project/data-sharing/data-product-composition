/**
 * Copyright (c) 2024 Centre for Research and Technology Hellas (CERTH) / Information Technologies Institute (ITI)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * SPDX-License-Identifier: MIT
 * 
 * Contributors:
 * Georgios Nikolaidis - Author
 * Vasileios Siopidis - Coauthor
 * Konstantinos Votis - Coauthor
 * Nikolaos Tepelidis - Coauthor
 */

import Joi from '@hapi/joi';
import moment from "moment";
import dbUtil from '../utils/db.util';
import { TrinoSettings } from '../models/trinoSettings.model';
import { BasicAuth, QueryData, Trino } from 'trino-client';
const fs = require('fs');
const path = require('path');

const createValidator = Joi.object({
    ipaddress: Joi.string().required()
});

class TrinoSettingsService {
    async get() {
        const connection = await dbUtil.getDefaultConnection();
        const catalogs = await connection.getRepository(TrinoSettings).findOne({
            where: {
                isDeleted: false
            }
        });
        return catalogs;
    }

    async save(model: any) {
        const validatedModel = await createValidator.validateAsync(model, { allowUnknown: true });
        const connection = await dbUtil.getDefaultConnection();
        let obj = await connection.getRepository(TrinoSettings).findOne({
            where: {
                isDeleted: false
            }
        });

        if (obj) {
            Object.assign(obj, validatedModel);
        }
        else {
            obj = new TrinoSettings();
            Object.assign(obj, validatedModel);
        }

        const newobj = await connection.manager.save(obj);
        return newobj;
    }


    async execute(model: any) {
        const connection = await dbUtil.getDefaultConnection();
        let obj = await connection.getRepository(TrinoSettings).findOne({
            where: {
                isDeleted: false
            }
        });
        if (!obj || !obj.query) {
            return null;
        }
        try {
            const trino: Trino = Trino.create({

                server: obj.url,
                catalog: 'tpcds',
                schema: 'sf100000',
                auth: new BasicAuth(obj.user),
            });
            let result = await trino.query(obj.query
                //'SELECT t1.id, t1.firstname, t1.lastname, t2.firstname, t2.lastname FROM postgresql.public.customer  t1 JOIN mariadb.trinodb.customer t2  on t1.id = t2.id  where t1.firstname != t2.firstname '
            );

            const data: QueryData[] = await result
                .map(r => r.data ?? [])
                .fold<QueryData[]>([], (row, acc) => [...acc, ...row]);

            // for await (const queryResult of result) {
            //     console.log(queryResult.data);
            // }
            return data;
        } catch (error) {
            console.log(error)
        }
    }







}

export default new TrinoSettingsService();