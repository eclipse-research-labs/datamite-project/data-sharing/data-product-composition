/**
 * Copyright (c) 2024 Centre for Research and Technology Hellas (CERTH) / Information Technologies Institute (ITI)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * SPDX-License-Identifier: MIT
 * 
 * Contributors:
 * Georgios Nikolaidis - Author
 * Vasileios Siopidis - Coauthor
 * Konstantinos Votis - Coauthor
 * Nikolaos Tepelidis - Coauthor
 */

import Joi from '@hapi/joi';
import moment from "moment";
import dbUtil from '../utils/db.util';
import { TrinoCatalog } from '../models/trinoCatalog.model';
import { TrinoSettings } from '../models/trinoSettings.model';
const fs = require('fs');
const path = require('path');

const createValidator = Joi.object({
    type: Joi.string().required()
});

class TrinoCatalogService {
    async getAll() {
        const connection = await dbUtil.getDefaultConnection();
        const catalogs = await connection.getRepository(TrinoCatalog).find({
            where: {
                isDeleted: false
            }
        });
        return catalogs;
    }


    async getById(id: any) {
        const connection = await dbUtil.getDefaultConnection();
        const catalog = await connection.getRepository(TrinoCatalog).findOne({ where: { id } });
        return catalog;
    }

    async create(model: any) {
        const validatedModel = await createValidator.validateAsync(model, { allowUnknown: true });
        const connection = await dbUtil.getDefaultConnection();
        const catalog = new TrinoCatalog();
        Object.assign(catalog, validatedModel);
        const obj = await connection.manager.save(catalog);
        await this.createCatalogFile(obj.fileName, obj);
        return obj;
    }

    async update(model: any) {
        const validatedModel = await createValidator.validateAsync(model, { allowUnknown: true });
        const connection = await dbUtil.getDefaultConnection();
        let id = model.id;
        const catalog = await connection.getRepository(TrinoCatalog).findOne({ where: { id } });
        Object.assign(catalog, validatedModel);
        const obj = await connection.manager.save(catalog);
        await this.createCatalogFile(obj.fileName, obj);
        return obj;
    }

    async delete(model: any) {
        const connection = await dbUtil.getDefaultConnection();
        let id = model.id;
        const catalog = await connection.getRepository(TrinoCatalog).findOne({ where: { id } });
        if (catalog) {
            catalog.isDeleted = true;
        }
        await this.deleteCatalogFile(catalog.fileName, catalog);
        const obj = await connection.manager.save(catalog);
        return obj;
    }

    async getRandomInt(min: number, max: number) {
        min = Math.ceil(min);
        max = Math.floor(max);
        return Math.floor(Math.random() * (max - min) + min);
    }


    async deleteCatalogFile(filename: string, model: TrinoCatalog) {
        const connection = await dbUtil.getDefaultConnection();
        let settings = await connection.getRepository(TrinoSettings).findOne({
            where: {
                isDeleted: false
            }
        });
        //filepath = "C:\\trinoserver\\etc";
        const fileSaveDirectory = settings.trinoCatalogPath; // path.join(filepath, 'catalog');
        if (!fs.existsSync(fileSaveDirectory)) {
            fs.mkdirSync(fileSaveDirectory);
        }

        const filePath = path.join(fileSaveDirectory, filename + ".properties"); // Full path for the file
        if (fs.existsSync(filePath)) {
            fs.unlinkSync(filePath);
            console.log(`Existing file "${filename}" deleted.`);
        }
    }

    async createCatalogFile(filename: string, model: TrinoCatalog) {

        const connection = await dbUtil.getDefaultConnection();
        let settings = await connection.getRepository(TrinoSettings).findOne({
            where: {
                isDeleted: false
            }
        });
        const fileSaveDirectory = settings.trinoCatalogPath;
        if (!fs.existsSync(fileSaveDirectory)) {
            fs.mkdirSync(fileSaveDirectory);
        }

        let fileContent = "";
        if (model.type === "mysql") {
            fileContent = "connector.name=mysql" + "\n";
            fileContent += "connection-url=jdbc:" + model.type + "://" + model.hostName + ":" + model.port + "\n";
            fileContent += "connection-user=" + model.user + "\n";
            fileContent += "connection-password=" + model.password + "\n";

        } else if (model.type === "mariadb") {
            fileContent = "connector.name=mysql" + "\n";
            fileContent += "connection-url=jdbc:mysql://" + model.hostName + ":" + model.port + "\n";
            fileContent += "connection-user=" + model.user + "\n";
            fileContent += "connection-password=" + model.password + "\n";
        }
        else if (model.type === "postgres") {
            fileContent = "connector.name=postgresql" + "\n";
            fileContent += "connection-url=jdbc:postgresql://" + model.hostName + ":" + model.port + "/" + model.dbName + "\n";
            fileContent += "connection-user=" + model.user + "\n";
            fileContent += "connection-password=" + model.password + "\n";
        }
        else if (model.type === "sqlserver") {

        }
        else if (model.type === "mongodb") {
            fileContent = "connector.name=mongodb" + "\n";
            fileContent += "connection-url=jdbc:mongodb://" + model.hostName + ":" + model.port + "/" + model.dbName + "\n";
            fileContent += "mongodb.schema-collection=trino.schemas";
            fileContent += "connection-password=" + model.password + "\n"

        }
        else if (model.type === "oracle") {

        }
        if (fs.existsSync(filename + ".properties")) {
            fs.unlinkSync(filename + ".properties");
            console.log(`Existing file "${filename}" deleted.`);
        }

        // Create a new .txt file
        const filePath = path.join(fileSaveDirectory, filename + ".properties"); // Full path for the file
        fs.writeFileSync(filePath, fileContent);
    }
}

export default new TrinoCatalogService();