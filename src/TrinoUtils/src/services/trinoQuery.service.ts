/**
 * Copyright (c) 2024 Centre for Research and Technology Hellas (CERTH) / Information Technologies Institute (ITI)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * SPDX-License-Identifier: MIT
 * 
 * Contributors:
 * Georgios Nikolaidis - Author
 * Vasileios Siopidis - Coauthor
 * Konstantinos Votis - Coauthor
 * Nikolaos Tepelidis - Coauthor
 */

import Joi from '@hapi/joi';
import moment from "moment";
import dbUtil from '../utils/db.util';
import { TrinoCatalog } from '../models/trinoCatalog.model';
import { TrinoSettings } from '../models/trinoSettings.model';
import { BasicAuth, QueryData, QueryResult, Trino } from 'trino-client';
import { TrinoQuery } from '../models/trinoQuery.model';
import { RecursiveTrinoQueryResult } from '../models/recursiveTrinoQueryResult.model';

import { writeFile } from 'fs';
const fs = require('fs');
const path = require('path');

const createValidator = Joi.object({
    query: Joi.string().required()
});




setInterval(async () => {
    const connection = await dbUtil.getDefaultConnection();
    const query = await connection.getRepository(TrinoQuery).createQueryBuilder("m")
        .where("m.isDeleted = :isDeleted").setParameter("isDeleted", false);
    query.andWhere("m.isRecursive = :isRecursive").setParameter("isRecursive", true);

    const queries = await query.getMany();
    if (queries && queries.length > 0) {
        let forExecute = [];
        for (let i = 0; i < queries.length; i++) {
            let trinoquery = queries[i];
            let now = new Date().toISOString();
            if (now > trinoquery.startDate.toISOString() && now.toString() < trinoquery.endDate.toISOString()) {
                if (trinoquery.executedDate == null || trinoquery.nextExecutedDate.toISOString() < now) {
                    let result = await executeRecursiveQuery(trinoquery);
                    trinoquery.executedDate = new Date(new Date().toUTCString());
                    let startdate = moment(trinoquery.executedDate);
                    let nextDate = moment(startdate).add(trinoquery.intervalHours, 'hours');
                    trinoquery.nextExecutedDate = new Date(nextDate.toISOString());
                    await connection.manager.save(trinoquery);

                    let trinoresult = new RecursiveTrinoQueryResult();
                    trinoresult.executedDate = trinoquery.executedDate;
                    trinoresult.nextExecutedDate = trinoquery.nextExecutedDate;
                    trinoresult.query = trinoquery;
                    trinoresult.queryId = trinoquery.id;
                    trinoresult.queryResult = JSON.stringify(result).toString();
                    await connection.manager.save(trinoresult);
                    console.log('HELLO G');
                }
            }
            //await deleteRecursiveQueryResults(trinoquery.id)
        }
    }

    console.log('HELLO GE');
}, 10000);

async function executeRecursiveQuery(trinoquery: TrinoQuery) {
    const connection = await dbUtil.getDefaultConnection();
    if (!trinoquery) {
        return null;
    }
    let obj = await connection.getRepository(TrinoSettings).findOne({
        where: {
            isDeleted: false
        }
    });
    if (!obj || !obj.query) {
        return null;
    }
    try {
        const trino: Trino = Trino.create({
            server: obj.url,
            catalog: 'tpcds',
            schema: 'sf100000',
            auth: new BasicAuth(obj.user),
        });
        let result = await trino.query(trinoquery.query);

        // const data: QueryData[] = await result
        //     .map(r => r.data ?? [])
        //     .fold<QueryData[]>([], (row, acc) => [...acc, ...row]);

        // for await (const queryResult of result) {
        //     console.log(queryResult.data);
        // }

        let arrData: QueryData[] = [];
        let columnNames = "";
        for await (const queryResult of result) {

            if (queryResult.columns && queryResult.columns.length && queryResult.columns.length > 0) {
                for (let i = 0; i < queryResult.columns.length; i++) {
                    columnNames += queryResult.columns[i].name;
                    if (i - 1 < queryResult.columns.length) {
                        columnNames += ",";
                    }
                }
            }
            if (queryResult.data && queryResult.data.length && queryResult.data.length > 0) {
                for (let i = 0; i < queryResult.data.length; i++) {
                    //columnNames += queryResult.data[i];
                    arrData.push(queryResult.data[i]);
                }
            }
        }

        const objResult = {
            columnNames: columnNames,
            data: arrData
        }

        return objResult;

    } catch (error) {
        console.log(error)
    }
}


async function deleteRecursiveQueryResults(queryId: any) {
    const connection = await dbUtil.getDefaultConnection();
    try {
        // Subquery to get the IDs of the 10 most recent posts
        const query = await connection.getRepository(RecursiveTrinoQueryResult).createQueryBuilder("m")
            .select("id")
            .where("m.queryId = : queryId").setParameter("id", queryId)
            .orderBy("m.executedDate", "DESC")
            .limit(10);


        // Delete all posts that are NOT in the recentPostsQuery
        const deletequery = await connection.getRepository(RecursiveTrinoQueryResult).createQueryBuilder("m")
            .delete()
            .from(RecursiveTrinoQueryResult)
            .where("m.queryId = : queryId").setParameter("queryId", queryId)
            .andWhere(`id NOT IN (${query.getQuery()})`)
            .setParameters(query.getParameters())
            .execute();

    } catch (error) {
        console.log(error)
    }
}



class TrinoQueryService {
    async getAll() {
        const connection = await dbUtil.getDefaultConnection();
        const catalogs = await connection.getRepository(TrinoQuery).find({
            where: {
                isDeleted: false
            }
        });
        return catalogs;
    }


    async getExecutedResults(queryId: any) {
        const connection = await dbUtil.getDefaultConnection();
        const query = connection.getRepository(RecursiveTrinoQueryResult).createQueryBuilder("m")
            .orderBy("m.executedDate", "DESC");
        query.andWhere("m.queryId = :queryId").setParameter("queryId", queryId);
        const results = await query.getMany();
        return results;

    }


    async exportExecutedResultOnCSV(id: any) {
        let queryResult = null;
        const connection = await dbUtil.getDefaultConnection();
        const query = connection.getRepository(RecursiveTrinoQueryResult).createQueryBuilder("m")
            .orderBy("m.executedDate", "DESC");
        query.andWhere("m.id = :id").setParameter("id", id);
        const result = await query.getOne();
        if (result) {


            queryResult = result.queryResult

        }
        return queryResult;

    }

    async getById(id: any) {
        const connection = await dbUtil.getDefaultConnection();
        const query = await connection.getRepository(TrinoQuery).findOne({ where: { id } });
        return query;
    }

    async create(model: any) {
        const validatedModel = await createValidator.validateAsync(model, { allowUnknown: true });
        const connection = await dbUtil.getDefaultConnection();
        const query = new TrinoQuery();
        Object.assign(query, validatedModel);
        const obj = await connection.manager.save(query);
        return obj;
    }

    async update(model: any) {
        const validatedModel = await createValidator.validateAsync(model, { allowUnknown: true });
        const connection = await dbUtil.getDefaultConnection();
        let id = model.id;
        const query = await connection.getRepository(TrinoQuery).findOne({ where: { id } });
        Object.assign(query, validatedModel);
        const obj = await connection.manager.save(query);
        return obj;
    }

    async delete(model: any) {
        const connection = await dbUtil.getDefaultConnection();
        let id = model.id;
        const query = await connection.getRepository(TrinoQuery).delete(id);
        // const obj = await connection.manager.save(query);
        // return obj;
    }

    async deleteResult(model: any) {
        const connection = await dbUtil.getDefaultConnection();
        let id = model.id;
        const query = await connection.getRepository(RecursiveTrinoQueryResult).delete(id);
        // const obj = await connection.manager.save(query);
        // return obj;
    }

    async getRandomInt(min: number, max: number) {
        min = Math.ceil(min);
        max = Math.floor(max);
        return Math.floor(Math.random() * (max - min) + min);
    }


    async execute(id: any) {
        const connection = await dbUtil.getDefaultConnection();
        const query = await connection.getRepository(TrinoQuery).findOne({ where: { id } });
        if (!query) {
            return null;
        }

        let obj = await connection.getRepository(TrinoSettings).findOne({
            where: {
                isDeleted: false
            }
        });
        if (!obj || !obj.query) {
            return null;
        }

        try {
            const trino: Trino = Trino.create({

                server: obj.url,
                catalog: 'tpcds',
                schema: 'sf100000',
                auth: new BasicAuth(obj.user),
            });
            let result = await trino.query(query.query
                //'SELECT t1.id, t1.firstname, t1.lastname, t2.firstname, t2.lastname FROM postgresql.public.customer  t1 JOIN mariadb.trinodb.customer t2  on t1.id = t2.id  where t1.firstname != t2.firstname '
            );

            // const data: QueryData[] = await result
            //     .map(r => r.data ?? [])
            //     .fold<QueryData[]>([], (row, acc) => [...acc, ...row]);

            let arrData: QueryData[] = [];
            let columnNames = "";
            for await (const queryResult of result) {

                if (queryResult.columns && queryResult.columns.length && queryResult.columns.length > 0) {
                    for (let i = 0; i < queryResult.columns.length; i++) {
                        columnNames += queryResult.columns[i].name;
                        if (i - 1 < queryResult.columns.length) {
                            columnNames += ",";
                        }
                    }
                }
                if (queryResult.data && queryResult.data.length && queryResult.data.length > 0) {
                    for (let i = 0; i < queryResult.data.length; i++) {
                        arrData.push(queryResult.data[i]);
                    }
                }
            }
            const objResult = {
                columnNames: columnNames,
                data: arrData
            }


            return objResult;

        } catch (error) {
            console.log(error)
        }
    }


    async executeInstant(queryString: any) {
        const connection = await dbUtil.getDefaultConnection();

        if (!queryString) {
            return null;
        }

        let obj = await connection.getRepository(TrinoSettings).findOne({
            where: {
                isDeleted: false
            }
        });
        if (!obj || !obj.query) {
            return null;
        }

        try {
            const trino: Trino = Trino.create({

                server: obj.url,
                catalog: 'tpcds',
                schema: 'sf100000',
                auth: new BasicAuth(obj.user),
            });
            let result = await trino.query(queryString
                //'SELECT t1.id, t1.firstname, t1.lastname, t2.firstname, t2.lastname FROM postgresql.public.customer  t1 JOIN mariadb.trinodb.customer t2  on t1.id = t2.id  where t1.firstname != t2.firstname '
            );

            // for await (const queryResult of result) {
            //     console.log(queryResult.data);
            // }

            // const data: QueryData[] = await result
            //     .map(r => r.data ?? [])
            //     .fold<QueryData[]>([], (row, acc) => [...acc, ...row]);

            let arrData: QueryData[] = [];
            let columnNames = "";
            for await (const queryResult of result) {

                if (queryResult.columns && queryResult.columns.length && queryResult.columns.length > 0) {
                    for (let i = 0; i < queryResult.columns.length; i++) {
                        columnNames += queryResult.columns[i].name;
                        if (i - 1 < queryResult.columns.length) {
                            columnNames += ",";
                        }
                    }
                }
                if (queryResult.data && queryResult.data.length && queryResult.data.length > 0) {
                    for (let i = 0; i < queryResult.data.length; i++) {
                        //columnNames += queryResult.data[i];
                        arrData.push(queryResult.data[i]);
                    }
                }

            }
            const objResult = {
                columnNames: columnNames,
                data: arrData
            }


            return objResult;
        } catch (error) {
            console.log(error)
        }
    }
}

export default new TrinoQueryService();