/**
 * Copyright (c) 2024 Centre for Research and Technology Hellas (CERTH) / Information Technologies Institute (ITI)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * SPDX-License-Identifier: MIT
 * 
 * Contributors:
 * Georgios Nikolaidis - Author
 * Vasileios Siopidis - Coauthor
 * Konstantinos Votis - Coauthor
 * Nikolaos Tepelidis - Coauthor
 */

import { NextFunction, Request, Response } from 'express';
import trinoQueryService from '../services/trinoQuery.service';
import responseUtil from '../utils/response.util';
import { ApiController } from './api.controller';
const { Readable } = require('stream');

class TrinoQueryController extends ApiController {
    async getAll(req: Request, res: Response, next: NextFunction) {
        try {
            const entities = await trinoQueryService.getAll();
            return res.json(responseUtil.createSuccessResponse(entities));
        } catch (error) {
            next(error);
        }
    }

    async getById(req: Request, res: Response, next: NextFunction) {
        try {
            const entities = await trinoQueryService.getById(Number(req.query.id));
            return res.json(responseUtil.createSuccessResponse(entities));
        } catch (error) {
            next(error);
        }
    }

    async create(req: Request, res: Response, next: NextFunction) {
        try {
            const entities = await trinoQueryService.create(req.body);
            return res.json(responseUtil.createSuccessResponse(entities));
        } catch (error) {
            next(error);
        }
    }

    async update(req: Request, res: Response, next: NextFunction) {
        try {
            const entities = await trinoQueryService.update(req.body);
            return res.json(responseUtil.createSuccessResponse(entities));
        } catch (error) {
            next(error);
        }
    }

    async delete(req: Request, res: Response, next: NextFunction) {
        try {
            const entities = await trinoQueryService.delete(req.body);
            return res.json(responseUtil.createSuccessResponse("Deleted query with id  : " + req.body.id));
        } catch (error) {
            next(error);
        }
    }

    async deleteResult(req: Request, res: Response, next: NextFunction) {
        try {
            const entities = await trinoQueryService.deleteResult(req.body);
            return res.json(responseUtil.createSuccessResponse("Deleted result with id  : " + req.body.id));
        } catch (error) {
            next(error);
        }
    }

    async execute(req: Request, res: Response, next: NextFunction) {
        try {
            const entities = await trinoQueryService.execute(req.query.id);
            return res.json(responseUtil.createSuccessResponse(entities));
        } catch (error) {
            next(error);
        }
    }

    async instantExecute(req: Request, res: Response, next: NextFunction) {
        try {
            const entities = await trinoQueryService.executeInstant(req.body.queryString);
            return res.json(responseUtil.createSuccessResponse(entities));
        } catch (error) {
            next(error);
        }
    }

    async getExecutedResults(req: Request, res: Response, next: NextFunction) {
        try {
            const entities = await trinoQueryService.getExecutedResults(req.query.queryId);
            return res.json(responseUtil.createSuccessResponse(entities));
        } catch (error) {
            next(error);
        }
    }


    async exportExecutedResultOnCSV(req: Request, res: Response, next: NextFunction) {
        try {

            res.setHeader('Content-Disposition', 'attachment; filename="output.csv"');
            res.setHeader('Content-Type', 'text/csv');
   
            // const header = 'ID,Name,Age\n';
            // const rows = data.map(item => `${item.id},${item.name},${item.age}`).join('\n');
            // const csv = header + rows;

        
            const stream = new Readable();
            const queryResult = await trinoQueryService.exportExecutedResultOnCSV(req.query.id);
            const jsonObject = JSON.parse(queryResult);
  
            let rowsData = jsonObject.columnNames + '\n';
            if (queryResult != null) {
                for (let i = 0; i < jsonObject.data.length; i++) {
                    let rowData = jsonObject.data[i];
                    console.log(rowData);
                    for (let j = 0; j < rowData.length; j++) {
                        console.log(rowData[j]);
                        rowsData += rowData[j] + ",";
                    }
                    rowsData += '\n';
                }
            }
            stream.push(rowsData);
            stream.push(null); // Signal end of stream
            stream.pipe(res);

            //return res.json(responseUtil.createSuccessResponse(entities));
        } catch (error) {
            next(error);
        }
    }

}



export default new TrinoQueryController();