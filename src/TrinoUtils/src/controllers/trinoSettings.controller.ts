/**
 * Copyright (c) 2024 Centre for Research and Technology Hellas (CERTH) / Information Technologies Institute (ITI)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * SPDX-License-Identifier: MIT
 * 
 * Contributors:
 * Georgios Nikolaidis - Author
 * Vasileios Siopidis - Coauthor
 * Konstantinos Votis - Coauthor
 * Nikolaos Tepelidis - Coauthor
 */

import { NextFunction, Request, Response } from 'express';
import trinoSettingsService from '../services/trinoSettings.service';
import responseUtil from '../utils/response.util';
import { ApiController } from './api.controller';
import { BasicAuth, QueryData, QueryResult, Trino } from 'trino-client';
//import TrinoClient from 'trino-client'; 

class TrinoSettingsController extends ApiController {
    async get(req: Request, res: Response, next: NextFunction) {
        try {
            const entities = await trinoSettingsService.get();
            return res.json(responseUtil.createSuccessResponse(entities));
        } catch (error) {
            next(error);
        }
    }

    async execute(req: Request, res: Response, next: NextFunction) {
        try {
            const entities = await trinoSettingsService.execute(req.query);
            return res.json(responseUtil.createSuccessResponse(entities));
        } catch (error) {
            next(error);
        }
    }


    async save(req: Request, res: Response, next: NextFunction) {
        try {
            const entities = await trinoSettingsService.save(req.body);
            //await this.testTrino();
            return res.json(responseUtil.createSuccessResponse(entities));
        } catch (error) {
            next(error);
        }
    }


    async testTrino() {
        try {
            const trino: Trino = Trino.create({

                server: 'http://160.40.55.166:8081',
                catalog: 'tpcds',
                schema: 'sf100000',
                auth: new BasicAuth('admin'),
            });

            let result = await trino.query(
                'SELECT t1.id, t1.firstname, t1.lastname, t2.firstname, t2.lastname FROM postgresql.public.customer  t1 JOIN mariadb.trinodb.customer t2  on t1.id = t2.id  where t1.firstname != t2.firstname '
            );

            const data: QueryData[] = await result
                .map(r => r.data ?? [])
                .fold<QueryData[]>([], (row, acc) => [...acc, ...row]);

            console.log(data);

            // for await (const queryResult of result) {
            //     console.log(queryResult.data);
            // }


            return data;
        } catch (error) {
            console.log(error)
        }
    }



}



export default new TrinoSettingsController();