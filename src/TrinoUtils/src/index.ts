/**
 * Copyright (c) 2024 Centre for Research and Technology Hellas (CERTH) / Information Technologies Institute (ITI)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * SPDX-License-Identifier: MIT
 */

import express from "express";
import dotenv from "dotenv";
import "reflect-metadata";
import * as routes from "./routes";
import { logger } from "./utils/logger.util";
import DbUtil from "./utils/db.util";
import { handle } from "./utils/error-handling.util";
import bodyParser from "body-parser";
import cors from "cors";
import helmet from "helmet";
import fileUpload from "express-fileupload";
import * as jwt from "jsonwebtoken";
import * as jwtJsDecode from 'jwt-js-decode';
dotenv.config({ path: ".env" });
import { helpers } from "./utils/helpers.util";
import responseUtil from './utils/response.util';
import { Keycloak } from "keycloak-backend"

const swaggerUi = require('swagger-ui-express');


// tslint:disable-next-line: no-var-requires


declare module "express" {
  interface Request {
    //user?: User;
  }
}

declare global {
  namespace CookieSessionInterfaces {
    interface CookieSessionObject {
      cartId: number;
    }
  }
}

// Initialize environment
dotenv.config();

// Initialize app
let app = express();
app.use(
  helmet({
    contentSecurityPolicy: {
      directives: {
        ...helmet.contentSecurityPolicy.getDefaultDirectives()
      }
    }
  })
);
const port = process.env.SERVER_PORT;

app.use(
  fileUpload({
    defCharset: "utf8",
    defParamCharset: "utf8"
  })
);


const corsOptions: cors.CorsOptions = {
  origin: "*",
  methods: ["GET", "POST", "PUT", "OPTIONS", "DELETE"],
  allowedHeaders: "*",
  optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204
};
app.use(cors(corsOptions));

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));

// parse application/json
app.use(bodyParser.json());

app.options("*", cors());

// Initialize database connections
DbUtil.init();

// Middleware
app.use(async (req, res, next) => {


  next();

});

const swaggerFile = require('../swagger_output.json');
app.use('/docs', swaggerUi.serve, swaggerUi.setup(swaggerFile));

// Register all routes
routes.registerRoutes(app);

// Handle un-handled errors
handle(app);


// start the express server
const server = app.listen(port, () => {
  logger.info(`server started at http://localhost:${port}`);
});






