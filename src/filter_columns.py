# Copyright (c) 2024 Centre for Research and Technology Hellas (CERTH) / Information Technologies Institute (ITI)
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
# 
# SPDX-License-Identifier: MIT
# 
# Contributors:
#     Nikolaos Tepelidis - Author
#     Vasileios Siopidis - Couthor
#     Georgios Nikolaidis - Couthor
#     Konstantinos Votis - Couthor

import pandas as pd
import io
import pandas as pd
from flask import jsonify, make_response

def get_datasets_logic(datasets, merged_dataset_by_columns):
    """
    Logic to retrieve datasets and their columns.

    Args:
        datasets: List of datasets (each dataset has a 'title' and 'dataframe').
        merged_dataset_by_columns: Merged DataFrame (if available).

    Returns:
        A list of datasets with their titles and column names.
    """
    try:
        # Initialize list to store column names and file names for each dataset
        csv_data = []

        # Iterate over imported datasets
        for dataset in datasets:
            title = dataset['title']
            column_names = list(dataset['dataframe'].columns)
            csv_data.append({'title': title, 'column_names': column_names})

        # Include the merged dataset if available
        if merged_dataset_by_columns is not None:
            csv_data.append({'title': 'merged_dataset', 'column_names': list(merged_dataset_by_columns.columns)})

        return csv_data

    except Exception as e:
        raise Exception(f"Error retrieving datasets: {str(e)}")
    


def download_filtered_logic(filtered_dfs, title, file_format):
    """
    Logic for downloading the filtered dataset in the specified format.

    Args:
        filtered_dfs: Dictionary of filtered DataFrames.
        title: Title of the dataset.
        file_format: Desired file format ('csv', 'xlsx', 'json').

    Returns:
        - Flask response object with the dataset in the requested format.
        - Error message if something goes wrong.
    """
    try:
        if title not in filtered_dfs:
            return None, 'Error: No filtered data available for the selected dataset. Please apply filters first.'

        filtered_df = filtered_dfs[title]

        # Create file-like object
        output_buffer = io.BytesIO()

        if file_format == 'csv':
            # Write DataFrame to CSV format
            filtered_df.to_csv(output_buffer, index=False)
            content_type = 'text/csv'
            file_extension = 'csv'
        elif file_format == 'xlsx':
            # Write DataFrame to Excel format
            filtered_df.to_excel(output_buffer, index=False)
            content_type = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
            file_extension = 'xlsx'
        elif file_format == 'json':
            # Write DataFrame to JSON format
            output_buffer.write(filtered_df.to_json(orient='records').encode())
            content_type = 'application/json'
            file_extension = 'json'
        else:
            return None, 'Unsupported file format'

        # Seek to the beginning of the buffer
        output_buffer.seek(0)

        # Create response
        response = make_response(output_buffer.getvalue())
        title = title.rsplit('.', 1)[0]
        # Set headers for file download
        response.headers["Content-Disposition"] = f"attachment; filename=filtered_dataset_{title}.{file_extension}"
        response.headers["Content-type"] = content_type

        return response, None
    except Exception as e:
        return None, str(e)    