# Copyright (c) 2024 Centre for Research and Technology Hellas (CERTH) / Information Technologies Institute (ITI)
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
# 
# SPDX-License-Identifier: MIT
# 
# Contributors:
#     Nikolaos Tepelidis - Author
#     Vasileios Siopidis - Couthor
#     Georgios Nikolaidis - Couthor
#     Konstantinos Votis - Couthor


import io
from flask import jsonify, make_response
from merge_datasets_by_columns_rows import merge_datasets_byrows_logic
import pandas as pd

def process_datasets(datasets, filtered_dfs, request_data):
    """
    Process datasets by merging them and then retrieving their information or deleting columns/rows.

    Args:
        datasets: List of imported datasets.
        filtered_dfs: Dictionary of filtered datasets.
        request_data: JSON data containing file specifications.

    Returns:
        A dictionary containing the merged dataset information or a deletion status.
    """
    datasets_to_merge = []

    # Call the merge function
    merged_df, message = merge_datasets_byrows_logic(datasets, filtered_dfs, request_data, datasets_to_merge)

    if isinstance(merged_df, pd.DataFrame):
        # Retrieve all datasets information
        all_datasets_info = get_all_datasets_logic(datasets, filtered_dfs, merged_df)

        # Example deletion data, replace with actual data you might receive
        deletions_data = {
            'deletions': [
                {'title': 'merged_dataset', 'columns': ['column_to_delete'], 'rows': []}  # Specify what to delete
            ]
        }

        # Perform deletions
        delete_status, code, last_merged_dataset = delete_columns_logic(datasets, merged_df, merged_df, filtered_dfs, deletions_data)

        return {
            'all_datasets_info': all_datasets_info,
            'delete_status': delete_status,
            'last_merged_dataset': last_merged_dataset
        }
    else:
        print("Error merging datasets:", message)
        return {'error': message}  # Return the error message

def get_all_datasets_logic(datasets, filtered_dfs, merged_dataset_by_columns):
    """
    Logic to retrieve all datasets and their columns.

    Args:
        datasets: List of imported datasets.
        filtered_dfs: Dictionary of filtered datasets.
        merged_dataset_by_columns: DataFrame of the merged dataset (if available).

    Returns:
        A list of datasets with their titles and column names.
    """
    try:
        # Initialize list to store column names and file names for each dataset
        csv_data = []

        # Iterate over imported datasets
        for dataset in datasets:
            title = dataset['title']
            column_names = list(dataset['dataframe'].columns)
            csv_data.append({'title': title, 'column_names': column_names})

        # Iterate over filtered datasets
        for title, filtered_df in filtered_dfs.items():
            column_names = list(filtered_df.columns)
            csv_data.append({'title': f'filtered_{title}', 'column_names': column_names})

        # Include the merged dataset if available
        if merged_dataset_by_columns is not None:
            csv_data.append({'title': 'merged_dataset', 'column_names': list(merged_dataset_by_columns.columns)})

        return csv_data

    except Exception as e:
        raise Exception(f"Error retrieving datasets: {str(e)}")
    

def delete_columns_logic(datasets, merged_dataset_by_columns, last_merged_dataset, filtered_dfs, data):
    """
    Logic for deleting specified columns or rows from datasets.

    Args:
        datasets: List of imported datasets.
        merged_dataset_by_columns: Merged DataFrame.
        last_merged_dataset: The last merged dataset to update.
        filtered_dfs: Dictionary of filtered DataFrames.
        data: JSON data from the POST request.

    Returns:
        A tuple containing:
        - Result (success or error message)
        - Status code
        - Updated last_merged_dataset
    """
    try:
        # Check if 'deletions' key exists in the JSON data
        if 'deletions' not in data:
            return {'status': 'error', 'message': 'No deletions specified'}, 400, last_merged_dataset

        # Iterate over deletions for each dataset
        for deletion in data['deletions']:
            title = deletion['title']
            columns_to_delete = deletion.get('columns', [])
            rows_to_delete = deletion.get('rows', [])

            if title == 'merged_dataset':
                # Handle deletion from the merged dataset
                if merged_dataset_by_columns is not None:
                    # Delete columns
                    if columns_to_delete:
                        invalid_columns = [col for col in columns_to_delete if col not in merged_dataset_by_columns.columns]
                        if invalid_columns:
                            return {'status': 'error', 'message': f'Columns {", ".join(invalid_columns)} do not exist in merged dataset'}, 400, last_merged_dataset
                        merged_dataset_by_columns = merged_dataset_by_columns.drop(columns=columns_to_delete, errors='ignore')

                    # Delete rows
                    if rows_to_delete:
                        merged_dataset_by_columns = delete_rows(merged_dataset_by_columns, rows_to_delete)

                    last_merged_dataset = merged_dataset_by_columns  # Update last_merged_dataset with new changes
                else:
                    return {'status': 'error', 'message': 'Merged dataset not found'}, 400, last_merged_dataset

            elif title.startswith('filtered_'):
                title = title.replace('filtered_', '')
                if title in filtered_dfs:
                    filtered_df = filtered_dfs[title]

                    # Delete columns
                    if columns_to_delete:
                        invalid_columns = [col for col in columns_to_delete if col not in filtered_df.columns]
                        if invalid_columns:
                            return {'status': 'error', 'message': f'Columns {", ".join(invalid_columns)} do not exist in {title}'}, 400, last_merged_dataset
                        filtered_dfs[title] = filtered_df.drop(columns=columns_to_delete, errors='ignore')

                    # Delete rows
                    if rows_to_delete:
                        filtered_dfs[title] = delete_rows(filtered_dfs[title], rows_to_delete)

                    last_merged_dataset = filtered_dfs[title]  # Update last_merged_dataset with new changes
                else:
                    return {'status': 'error', 'message': f'Filtered dataset {title} not found'}, 400, last_merged_dataset

            else:
                dataset = next((d for d in datasets if d['title'] == title), None)

                if dataset:
                    # Delete columns
                    if columns_to_delete:
                        invalid_columns = [col for col in columns_to_delete if col not in dataset['dataframe'].columns]
                        if invalid_columns:
                            return {'status': 'error', 'message': f'Columns {", ".join(invalid_columns)} do not exist in {title}'}, 400, last_merged_dataset
                        dataset['dataframe'] = dataset['dataframe'].drop(columns=columns_to_delete, errors='ignore')

                    # Delete rows
                    if rows_to_delete:
                        dataset['dataframe'] = delete_rows(dataset['dataframe'], rows_to_delete)

                    last_merged_dataset = dataset['dataframe']  # Update last_merged_dataset with new changes

        return {'status': 'success', 'message': 'Columns and/or rows deleted successfully'}, 200, last_merged_dataset, merged_dataset_by_columns

    except Exception as e:
        return {'status': 'error', 'message': str(e)}, 400, last_merged_dataset, merged_dataset_by_columns

def delete_rows(df, rows_to_delete):
    """
    Delete specific rows or a range of rows from the DataFrame.

    rows_to_delete can contain specific row indices or ranges, e.g., [12, 15, 30] or ["12-30"].
    The row indices provided by the user are assumed to start from 2 (i.e., user-visible row numbers start at 2).
    """
    rows_to_drop = []

    # Validate if any row is 0 or 1
    for row in rows_to_delete:
        if isinstance(row, str) and '-' in row:
            # Handle range of rows (e.g., "12-30")
            start, end = map(int, row.split('-'))
            if start < 2 or end < 2:
                return {'status': 'error', 'message': 'Row numbers must start from 2 and above.'}, 400
            rows_to_drop.extend(range(start - 2, end - 2 + 1))  # Adjust the range to start from 0
        elif isinstance(row, int):
            # Handle specific row index, check if it's less than 2
            if row < 2:
                return {'status': 'error', 'message': 'Row numbers must start from 2 and above.'}, 400
            rows_to_drop.append(row - 2)

    # Drop rows by index
    df = df.drop(rows_to_drop, errors='ignore')
    return df  

def enforce_integer_columns(df):
    """Ensure that columns that should be integers remain as integers."""
    for col in df.select_dtypes(include=['float']):
        # Check if all non-NaN values in the column are integers
        if all(df[col].dropna().apply(float.is_integer)):
            # Convert to int but keep NaNs
            df[col] = df[col].astype('Int64')  # Use 'Int64' for nullable integers
    return df

def download_deleted_dataset_logic(datasets, filtered_dfs, merged_dataset_by_columns, title, file_format, enforce_integer_columns):
    """
    Logic to download a dataset in the specified format (CSV, Excel, or JSON).

    Args:
        datasets: List of available datasets.
        filtered_dfs: Dictionary of filtered DataFrames.
        merged_dataset_by_columns: Merged dataset (if any).
        title: Title of the dataset to download.
        file_format: The format in which to download the dataset (CSV, Excel, JSON).

    Returns:
        A tuple containing:
        - A Flask response object with the dataset in the specified format.
        - A status code (200 for success, 400 or 500 for errors).
    """
    try:
        # Check if the title is provided
        if not title:
            return jsonify({'status': 'error', 'message': 'File name not specified'}), 400

        # Check if there are datasets available
        if not datasets and not filtered_dfs and merged_dataset_by_columns is None:
            return jsonify({'status': 'error', 'message': 'No datasets to download'}), 400

        # Remove existing file extensions from the file name
        file_name_without_extension = title.rsplit('.', 1)[0]

        # Find the specified dataset
        if title == 'merged_dataset':
            dataframe = merged_dataset_by_columns
        elif title.startswith('filtered_'):
            original_file_name = title.replace('filtered_', '')
            dataframe = filtered_dfs.get(original_file_name)
        else:
            dataset = next((d for d in datasets if d['title'] == title), None)
            dataframe = dataset['dataframe'] if dataset else None

        # Check if the dataset is found
        if dataframe is None:
            return jsonify({'status': 'error', 'message': f'Dataset {title} not found'}), 400

        # Ensure the DataFrame has integer columns enforced (Assume enforce_integer_columns is defined elsewhere)
        dataframe = enforce_integer_columns(dataframe)

        # Create file-like object
        output_buffer = io.BytesIO()

        # Generate the file in the specified format
        if file_format == 'csv':
            dataframe.to_csv(output_buffer, index=False, float_format='%.0f', sep=',')
            content_type = 'text/csv'
            file_extension = 'csv'
        elif file_format == 'xlsx':
            dataframe.to_excel(output_buffer, index=False)
            content_type = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
            file_extension = 'xlsx'
        elif file_format == 'json':
            output_buffer.write(dataframe.to_json(orient='records').encode())
            content_type = 'application/json'
            file_extension = 'json'
        else:
            return jsonify({'status': 'error', 'message': 'Unsupported file format'}), 400

        # Seek to the beginning of the buffer
        output_buffer.seek(0)

        # Create a response object
        response = make_response(output_buffer.getvalue())
        response.headers["Content-Disposition"] = f"attachment; filename={file_name_without_extension}.{file_extension}"
        response.headers["Content-type"] = content_type

        return response, 200

    except Exception as e:
        return jsonify({'status': 'error', 'message': str(e)}), 500
    

def retain_columns_logic(datasets, merged_dataset_by_columns, last_merged_dataset, filtered_dfs, data):
    """
    Retain columns and rows endpoint.

    Accepts POST requests to retain specified columns or rows from imported CSV files.
    """
    try:


        # Check if 'retentions' key exists in the JSON data
        if 'retentions' not in data:
            return jsonify({'status': 'error', 'message': 'No retentions specified'}), 400

        # Iterate over retentions for each CSV
        for retention in data['retentions']:
            title = retention['title']
            columns_to_retain = retention.get('columns', [])
            rows_to_retain = retention.get('rows', [])

            if title == 'merged_dataset':
                # Handle retention in the merged_dataset_by_columns
                if merged_dataset_by_columns is not None:
                    # Retain columns
                    if columns_to_retain:
                        invalid_columns = [col for col in columns_to_retain if col not in merged_dataset_by_columns.columns]
                        if invalid_columns:
                            return jsonify({'status': 'error', 'message': f'Columns {", ".join(invalid_columns)} do not exist in merged dataset'}), 400
                        columns_to_drop = [col for col in merged_dataset_by_columns.columns if col not in columns_to_retain]
                        merged_dataset_by_columns = merged_dataset_by_columns.drop(columns=columns_to_drop, errors='ignore')

                    # Retain rows
                    if rows_to_retain:
                        merged_dataset_by_columns = retain_rows(merged_dataset_by_columns, rows_to_retain)

                    last_merged_dataset = merged_dataset_by_columns
                else:
                    return jsonify({'status': 'error', 'message': 'Merged dataset not found'}), 400

            elif title.startswith('filtered_'):
                title = title.replace('filtered_', '')
                if title in filtered_dfs:
                    filtered_df = filtered_dfs[title]
                    
                    # Retain columns
                    if columns_to_retain:
                        invalid_columns = [col for col in columns_to_retain if col not in filtered_df.columns]
                        if invalid_columns:
                            return jsonify({'status': 'error', 'message': f'Columns {", ".join(invalid_columns)} do not exist in {title}'}), 400
                        columns_to_drop = [col for col in filtered_df.columns if col not in columns_to_retain]
                        filtered_dfs[title] = filtered_df.drop(columns=columns_to_drop, errors='ignore')

                    # Retain rows
                    if rows_to_retain:
                        filtered_dfs[title] = retain_rows(filtered_dfs[title], rows_to_retain)

                    last_merged_dataset = filtered_dfs[title]
                else:
                    return jsonify({'status': 'error', 'message': f'Filtered dataset {title} not found'}), 400

            else:
                dataset = next((d for d in datasets if d['title'] == title), None)

                if dataset:
                    # Retain columns
                    if columns_to_retain:
                        invalid_columns = [col for col in columns_to_retain if col not in dataset['dataframe'].columns]
                        if invalid_columns:
                            return jsonify({'status': 'error', 'message': f'Columns {", ".join(invalid_columns)} do not exist in {title}'}), 400
                        columns_to_drop = [col for col in dataset['dataframe'].columns if col not in columns_to_retain]
                        dataset['dataframe'] = dataset['dataframe'].drop(columns=columns_to_drop, errors='ignore')

                    # Retain rows
                    if rows_to_retain:
                        dataset['dataframe'] = retain_rows(dataset['dataframe'], rows_to_retain)

                    last_merged_dataset = dataset['dataframe']

        return {'status': 'success', 'message': 'Columns and/or rows deleted successfully'}, 200, last_merged_dataset, merged_dataset_by_columns

    except Exception as e:
        return {'status': 'error', 'message': str(e)}, 400, last_merged_dataset, merged_dataset_by_columns
    

def retain_rows(df, rows_to_retain):
    """
    Retain specific rows or a range of rows from the DataFrame.

    rows_to_retain can contain specific row indices or ranges, e.g., [12, 15, 30] or ["12-30"].
    The row indices provided by the user are assumed to start from 2 (i.e., user-visible row numbers start at 2).
    """
    rows_to_keep = []

    # Validate and process row retention
    for row in rows_to_retain:
        if isinstance(row, str) and '-' in row:
            # Handle range of rows (e.g., "12-30")
            start, end = map(int, row.split('-'))
            if start < 2 or end < 2:
                return {'status': 'error', 'message': 'Row numbers must start from 2 and above.'}, 400
            rows_to_keep.extend(range(start - 2, end - 2 + 1))  # Adjust the range to start from 0
        elif isinstance(row, int):
            # Handle specific row index, check if it's less than 2
            if row < 2:
                return {'status': 'error', 'message': 'Row numbers must start from 2 and above.'}, 400
            rows_to_keep.append(row - 2)

    # Retain only the specified rows by index
    df = df.iloc[rows_to_keep]
    return df  # Return only the modified DataFrame
        
def download_dataset_logic(datasets, filtered_dfs, merged_dataset_by_columns, title, format, enforce_integer_columns):
    """
    Logic to download the retained dataset in the specified format.

    Args:
        datasets: List of datasets.
        filtered_dfs: Dictionary of filtered DataFrames.
        merged_dataset_by_columns: Merged dataset DataFrame.
        title: The name of the dataset to download.
        format: The format in which to download the dataset (csv, xlsx, or json).
        enforce_integer_columns: Function to enforce integer columns in the DataFrame.

    Returns:
        A Flask response object with the dataset file or an error message.
    """
    try:
        # Check if title is provided
        if not title:
            return jsonify({'status': 'error', 'message': 'File name not specified'}), 400

        # Check if datasets are available to download
        if not datasets and not filtered_dfs and merged_dataset_by_columns is None:
            return jsonify({'status': 'error', 'message': 'No datasets to download'}), 400

        # Remove existing file extensions from the file name
        file_name_without_extension = title.rsplit('.', 1)[0]

        # Find the specified dataset
        if title == 'merged_dataset':
            dataframe = merged_dataset_by_columns
        elif title.startswith('filtered_'):
            original_file_name = title.replace('filtered_', '')
            dataframe = filtered_dfs.get(original_file_name)
        else:
            dataset = next((d for d in datasets if d['title'] == title), None)
            dataframe = dataset['dataframe'] if dataset else None

        # Check if the dataset is found
        if dataframe is None:
            return jsonify({'status': 'error', 'message': f'Dataset {title} not found'}), 400

        # Ensure the DataFrame has integer columns enforced
        dataframe = enforce_integer_columns(dataframe)

        # Create file-like object
        output_buffer = io.BytesIO()

        if format == 'csv':
            # Write DataFrame to CSV format
            dataframe.to_csv(output_buffer, index=False)
            content_type = 'text/csv'
            file_extension = 'csv'
        elif format == 'xlsx':
            # Write DataFrame to Excel format
            dataframe.to_excel(output_buffer, index=False)
            content_type = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
            file_extension = 'xlsx'
        elif format == 'json':
            # Write DataFrame to JSON format
            output_buffer.write(dataframe.to_json(orient='records').encode())
            content_type = 'application/json'
            file_extension = 'json'
        else:
            return jsonify({'status': 'error', 'message': 'Unsupported file format'}), 400

        # Seek to the beginning of the buffer
        output_buffer.seek(0)

        # Create response
        response = make_response(output_buffer.getvalue())

        # Set headers for file download
        response.headers["Content-Disposition"] = f"attachment; filename={file_name_without_extension}.{file_extension}"
        response.headers["Content-type"] = content_type

        return response

    except Exception as e:
        return jsonify({'status': 'error', 'message': str(e)}), 500        