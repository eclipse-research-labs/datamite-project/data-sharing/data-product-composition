# Copyright (c) 2024 Centre for Research and Technology Hellas (CERTH) / Information Technologies Institute (ITI)
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
# 
# SPDX-License-Identifier: MIT
# 
# Contributors:
#     Nikolaos Tepelidis - Author
#     Vasileios Siopidis - Couthor
#     Georgios Nikolaidis - Couthor
#     Konstantinos Votis - Couthor


import pandas as pd
import io
import json
from flask import jsonify, make_response

def enforce_integer_columns(df):
    """Ensure that columns that should be integers remain as integers."""
    for col in df.select_dtypes(include=['float']):
        # Check if all non-NaN values in the column are integers
        if all(df[col].dropna().apply(float.is_integer)):
            # Convert to int but keep NaNs
            df[col] = df[col].astype('Int64')  # Use 'Int64' for nullable integers
    return df
# Function to check if datasets have identical column names    
def check_identical_column_names(datasets):
    all_columns = set() # Initialize an empty set to store column names
    for dataset in datasets: # Iterate over datasets
        columns = set(dataset['dataframe'].columns) # Get the set of column names for the current dataset
        if all_columns.intersection(columns): # Check if there are common column names
            return True # Return True if common column names are found
        all_columns.update(columns) # Update the set of all column names with the current dataset's column names
    return False # Return False if no common column names are found

#BY PRIMARY KEY

def validate_and_merge_datasets_byPrimaryKey(datasets, primary_key):
    """
    Validates and merges datasets based on a common primary key.

    Args:
        datasets (list): List of datasets (each containing 'title' and 'dataframe').
        primary_key (str): The column name that serves as the primary key for merging.

    Returns:
        tuple: (merged DataFrame or None, message)
    """
    try:
        # Ensure there are at least two datasets
        if len(datasets) < 2:
            return None, "At least two datasets are required to perform a merge."

        # Validate that all datasets contain the primary key
        for ds in datasets:
            if primary_key not in ds['dataframe'].columns:
                return None, f"Primary key '{primary_key}' not found in dataset '{ds['title']}'."

        # Check for column conflicts before merging
        common_columns = set(datasets[0]['dataframe'].columns)
        for ds in datasets[1:]:
            common_columns &= set(ds['dataframe'].columns)

        # Exclude the primary key from the columns to check for conflicts
        common_columns -= {primary_key}

        # Check for conflicting values in common columns
        for col in common_columns:
            combined_values = {}
            for ds in datasets:
                df = ds['dataframe']
                if col in df.columns:
                    # Extract the primary key and corresponding column values
                    for key, value in df[[primary_key, col]].dropna().values:
                        if key in combined_values:
                            if combined_values[key] != value:
                                return None, f"Conflict detected in column '{col}' for primary key value '{key}' between datasets."
                        else:
                            combined_values[key] = value

        # Merge datasets on the primary key
        merged_df = datasets[0]['dataframe']
        for i in range(1, len(datasets)):
            merged_df = pd.merge(
                merged_df,
                datasets[i]['dataframe'],
                on=primary_key,
                how='inner',  # Keep only rows where the primary key matches across datasets
                suffixes=('', f'_ds{i}')  # Avoid column name conflicts
            )

        if merged_df.empty:
            return None, "No matching rows found based on the primary key."

        return merged_df, "Datasets merged successfully by primary key."

    except Exception as e:
        return None, f"An error occurred during merging: {str(e)}"

def merge_datasets_byPrimaryKey_logic(datasets, filtered_dfs, request_data, datasets_to_merge):
    print("Starting merging by primary key")
    try:
        if not datasets and not filtered_dfs:
            return None, jsonify({'status': 'error', 'message': 'No files uploaded'}), 400

        files_to_merge = request_data.get('files', [])
        primary_key = request_data.get('primary_key')

        if not files_to_merge:
            return None, jsonify({'status': 'error', 'message': 'No files specified for merging'}), 400

        if not primary_key:
            return None, jsonify({'status': 'error', 'message': 'Primary key not specified'}), 400

        # Filter the datasets to merge based on user input
        for ds in datasets:
            if ds['title'] in files_to_merge:
                datasets_to_merge.append(ds)

        for title, filtered_df in filtered_dfs.items():
            if f'filtered_{title}' in files_to_merge:
                datasets_to_merge.append({'title': f'filtered_{title}', 'dataframe': filtered_df})

        if len(datasets_to_merge) < 2:
            return None, jsonify({'status': 'error', 'message': 'At least two valid files must be specified for merging'}), 400

        # Perform the dataset merge by primary key
        merged_df, message = validate_and_merge_datasets_byPrimaryKey(datasets_to_merge, primary_key)

        if merged_df is None:
            return None, jsonify({'status': 'error', 'message': message}), 400
        elif not isinstance(merged_df, pd.DataFrame):
            return None, jsonify({'status': 'error', 'message': 'Merged data is not a DataFrame'}), 400
        else:
            return merged_df, jsonify({'status': 'success', 'message': message}), 200

    except Exception as e:
        # Catch any exceptions and ensure two values are returned
        return None, jsonify({'status': 'error', 'message': f"An unexpected error occurred: {str(e)}"}), 500
    
    
def download_merged_dataset_byPrimaryKey_logic(datasets, datasets_to_merge, enforce_integer_columns, request):
    """
    Logic for downloading the merged dataset.

    Args:
        datasets_to_merge: List of datasets that have already been merged or need to be merged.
        enforce_integer_columns: Function to enforce integer columns on the DataFrame.
        request: Flask request object to get format from query parameters.

    Returns:
        File download response or error message.
    """
    try:
        # Ensure there are datasets to download
        if not datasets_to_merge:
            return jsonify({'status': 'error', 'message': 'No datasets to download'}), 400

        # If datasets_to_merge is not already merged, validate and merge them
        if isinstance(datasets_to_merge, list):
            merged_data = datasets_to_merge[0]['dataframe']
            for i in range(1, len(datasets_to_merge)):
                merged_data = pd.merge(
                    merged_data,
                    datasets_to_merge[i]['dataframe'],
                    how='inner'  # Use inner join to merge datasets
                )
        else:
            merged_data = datasets_to_merge

        # Ensure merged_data is a DataFrame
        if not isinstance(merged_data, pd.DataFrame):
            try:
                merged_data = pd.DataFrame(merged_data)
            except Exception as e:
                error_message = f"Error converting merged_data to DataFrame: {str(e)}"
                return jsonify({'status': 'error', 'message': error_message}), 500

        # Enforce integer columns if needed
        merged_data = enforce_integer_columns(merged_data)

        # Get the desired file format from query parameters
        file_format = request.args.get('format', 'csv').lower()

        # Create file-like object
        output_buffer = io.BytesIO()

        if file_format == 'csv':
            merged_data.to_csv(output_buffer, index=False, float_format='%.0f')
            content_type = 'text/csv'
            file_extension = 'csv'
        elif file_format == 'xlsx':
            merged_data.to_excel(output_buffer, index=False)
            content_type = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
            file_extension = 'xlsx'
        elif file_format == 'json':
            json_data = merged_data.to_dict(orient='records')
            output_buffer.write(json.dumps(json_data).encode())
            content_type = 'application/json'
            file_extension = 'json'
        else:
            return jsonify({'status': 'error', 'message': 'Unsupported file format'}), 400

        # Seek to the beginning of the buffer
        output_buffer.seek(0)

        # Create the response
        response = make_response(output_buffer.getvalue())
        response.headers["Content-Disposition"] = f"attachment; filename=merged_dataset.{file_extension}"
        response.headers["Content-type"] = content_type

        return response

    except Exception as e:
        return jsonify({'status': 'error', 'message': str(e)}), 500


#BY COLUMNS

def validate_and_merge_datasets_bycolumns(datasets):
    global merged_dataset_by_columns
    print("Starting validation of datasets for merging")
    """
    Validate and merge datasets by columns.

    Args:
        datasets (list): A list of dictionaries, each containing information about a dataset.

    Returns:
        tuple: A tuple containing the merged DataFrame and a message indicating the status of the operation.
    """
    try:
        if len(datasets) < 2:
            return None, "Insufficient datasets to merge"  # Return if there are not enough datasets

        first_df = datasets[0]['dataframe']  # Get the DataFrame of the first dataset

        # Check for data length consistency
        first_rows_count = first_df.shape[0]  # Get the number of rows in the first DataFrame
        if not all(first_rows_count == data['dataframe'].shape[0] for data in datasets[1:]):
            return None, "Datasets have inconsistent numbers of rows."

        # Check for identical column names in different datasets    
        for i in range(len(datasets)):
            for j in range(i + 1, len(datasets)):
                if check_identical_column_names([datasets[i], datasets[j]]):
                    return None, "Datasets have identical column names. Merging not necessary."

        # Merge datasets by concatenating DataFrames along columns
        merged_df = pd.concat([data['dataframe'] for data in datasets], axis=1)  # Concatenate DataFrames
        if merged_df.columns.duplicated().any():
            duplicate_columns = merged_df.columns[merged_df.columns.duplicated()].unique()
            raise ValueError(f"Duplicate columns found: {', '.join(duplicate_columns)}")

        merged_df = enforce_integer_columns(merged_df)
        merged_dataset_by_columns = merged_df
        return merged_df, "Datasets merged successfully."
    
    except ValueError as ve:
        # Handle known errors, e.g., duplicate columns
        return None, f"Merge error: {str(ve)}"
    except TypeError as te:
        # Handle type errors in concatenation or column validation
        return None, f"Type error during merge: {str(te)}"
    except Exception as e:
        # General error handling
        return None, f"Unexpected error during merge: {str(e)}"
    
def show_datasets_logic(datasets, filtered_dfs):
    """
    Function to show imported datasets.

    Args:
        datasets: A list of datasets that have been uploaded.
        filtered_dfs: A dictionary of filtered DataFrames.

    Returns:
        JSON response with the file names and column names of imported datasets.
    """
    try:
        # Initialize lists to store column names and file names for each CSV
        csv_data = []

        # Iterate over imported datasets
        for dataset in datasets:
            title = dataset['title']
            column_names = list(dataset['dataframe'].columns)
            csv_data.append({'title': title, 'column_names': column_names})
        
        # Add filtered datasets
        for title, filtered_df in filtered_dfs.items():
            column_names = list(filtered_df.columns)
            csv_data.append({'title': f'filtered_{title}', 'column_names': column_names})

        return jsonify({'status': 'success', 'csv_data': csv_data}), 200

    except Exception as e:
        return jsonify({'status': 'error', 'message': str(e)}), 400
    

def merge_datasets_bycolumns_logic(datasets, filtered_dfs, request_data, datasets_to_merge):
    print("Starting merging")
    try:
        if not datasets and not filtered_dfs:
            return None, jsonify({'status': 'error', 'message': 'No files uploaded'}), 400

        files_to_merge = request_data.get('files', [])
        if not files_to_merge:
            return None, jsonify({'status': 'error', 'message': 'No files specified for merging'}), 400

        # Filter the datasets to merge based on user input
        for ds in datasets:
            if ds['title'] in files_to_merge:
                datasets_to_merge.append(ds)

        for title, filtered_df in filtered_dfs.items():
            if f'filtered_{title}' in files_to_merge:
                datasets_to_merge.append({'title': f'filtered_{title}', 'dataframe': filtered_df})

        if len(datasets_to_merge) < 2:
            return None, jsonify({'status': 'error', 'message': 'At least two valid files must be specified for merging'}), 400

        # Perform the dataset merge
        merged_df, message = validate_and_merge_datasets_bycolumns(datasets_to_merge)

        if merged_df is None:
            return None, jsonify({'status': 'error', 'message': message}), 400
        elif not isinstance(merged_df, pd.DataFrame):
            return None, jsonify({'status': 'error', 'message': 'Merged data is not a DataFrame'}), 400
        else:
            return merged_df, jsonify({'status': 'success', 'message': message}), 200

    except Exception as e:
        # Catch any exceptions and ensure two values are returned
        return None, jsonify({'status': 'error', 'message': f"An unexpected error occurred: {str(e)}"}), 500


def download_merged_dataset_bycolumns_logic(datasets, datasets_to_merge, enforce_integer_columns, request):
    """
    Logic for downloading the merged dataset by columns.

    Args:
        datasets: A list of datasets that have been uploaded.
        datasets_to_merge: List of datasets to merge.
        enforce_integer_columns: Function to enforce integer columns on the DataFrame.
        request: Flask request object to get format from query parameters.

    Returns:
        File download response or error message.
    """
    try:
        if not datasets:
            return jsonify({'status': 'error', 'message': 'No datasets to download'}), 400

        # Validate and merge datasets
        merged_data, message = validate_and_merge_datasets_bycolumns(datasets_to_merge)
        if merged_data is None:
            return jsonify({'status': 'error', 'message': message}), 400

        if not isinstance(merged_data, pd.DataFrame):
            try:
                merged_data = pd.DataFrame(merged_data)
            except Exception as e:
                error_message = f"Error converting merged_data to DataFrame: {str(e)}"
                return jsonify({'status': 'error', 'message': error_message}), 500

        # Enforce integer columns if needed
        merged_data = enforce_integer_columns(merged_data)

        # Get the desired file format from query parameters
        file_format = request.args.get('format', 'csv').lower()

        # Create file-like object
        output_buffer = io.BytesIO()

        if file_format == 'csv':
            merged_data.to_csv(output_buffer, index=False, float_format='%.0f')
            content_type = 'text/csv'
            file_extension = 'csv'
        elif file_format == 'xlsx':
            merged_data.to_excel(output_buffer, index=False)
            content_type = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
            file_extension = 'xlsx'
        elif file_format == 'json':
            json_data = merged_data.to_dict(orient='records')
            output_buffer.write(json.dumps(json_data).encode())
            content_type = 'application/json'
            file_extension = 'json'
        else:
            return jsonify({'status': 'error', 'message': 'Unsupported file format'}), 400

        # Seek to the beginning of the buffer
        output_buffer.seek(0)

        # Create the response
        response = make_response(output_buffer.getvalue())
        response.headers["Content-Disposition"] = f"attachment; filename=merged_dataset.{file_extension}"
        response.headers["Content-type"] = content_type

        return response

    except Exception as e:
        return jsonify({'status': 'error', 'message': str(e)}), 500     
    
#BY ROWS

def validate_and_merge_datasets_byrows(datasets):
    global merged_dataset_by_columns
    try:
        if len(datasets) < 2:
            return None, "Insufficient datasets to merge"

        # Ensure all datasets have the same columns, regardless of order
        reference_columns = set(datasets[0]['dataframe'].columns)  # Collect column names of the first dataset
        for i in range(1, len(datasets)):
            current_columns = set(datasets[i]['dataframe'].columns)
            if reference_columns != current_columns:
                return None, f"Datasets have non-identical column names."

        # Reorder columns to match the first dataset's column order
        for dataset in datasets:
            dataset['dataframe'] = dataset['dataframe'][list(datasets[0]['dataframe'].columns)]

        # Check for compatible data types within each column
        for i in range(len(datasets)):
            for j in range(i + 1, len(datasets)):
                for col_name in datasets[0]['dataframe'].columns:
                    col_data_types_i = datasets[i]['dataframe'][col_name].dtype
                    col_data_types_j = datasets[j]['dataframe'][col_name].dtype
                    if col_data_types_i != col_data_types_j:
                        return None, f"Incompatible data types in column '{col_name}' between datasets {i+1} and {j+1}"

        # Merge datasets by rows
        merged_df = pd.concat([data['dataframe'] for data in datasets], axis=0, ignore_index=True)

        if merged_df.columns.duplicated().any():
            duplicate_columns = merged_df.columns[merged_df.columns.duplicated()].unique()
            raise ValueError(f"Duplicate columns found: {', '.join(duplicate_columns)}")

        # Enforce integer columns if required
        merged_df = enforce_integer_columns(merged_df)
        merged_dataset_by_columns = merged_df
        return merged_df, "Datasets merged successfully."
    except TypeError as e:
        # Extract column names from the exception message (if possible)
        incompatible_cols = [str(s) for s in str(e).split() if s.isalnum()]  # Extract alphanumeric parts
        message = f"Datasets have incompatible data types in some columns: {', '.join(incompatible_cols)}"
        return None, message
    


def merge_datasets_byrows_logic(datasets, filtered_dfs, request_data, datasets_to_merge):
    try:
        if not datasets and not filtered_dfs:
            return None, jsonify({'status': 'error', 'message': 'No files uploaded'}), 400

        files_to_merge = request_data.get('files', [])
        if not files_to_merge:
            return None, jsonify({'status': 'error', 'message': 'No files specified for merging'}), 400

        # Filter the datasets to merge based on user input
        for ds in datasets:
            if ds['title'] in files_to_merge:
                datasets_to_merge.append(ds)

        for title, filtered_df in filtered_dfs.items():
            if f'filtered_{title}' in files_to_merge:
                datasets_to_merge.append({'title': f'filtered_{title}', 'dataframe': filtered_df})

        if len(datasets_to_merge) < 2:
            return None, jsonify({'status': 'error', 'message': 'At least two valid files must be specified for merging'}), 400

        merged_df, message = validate_and_merge_datasets_byrows(datasets_to_merge)  # Validate and merge datasets
        if merged_df is None:
            return None, jsonify({'status': 'error', 'message': message}), 400
        elif not isinstance(merged_df, pd.DataFrame):
            return None, jsonify({'status': 'error', 'message': 'Merged data is not a DataFrame'}), 400
        else:
            return merged_df, jsonify({'status': 'success', 'message': message}), 200  # Add status code 200 for success

    except Exception as e:  # Catch any exception
        return None, jsonify({'status': 'error', 'message': f"An unexpected error occurred: {str(e)}"}), 500
    
def download_merged_dataset_byrows_logic(datasets, datasets_to_merge, enforce_integer_columns, request):

    try:
        if not datasets:
            return jsonify({'status': 'error', 'message': 'No datasets to download'}), 400

        # Validate and merge datasets
        merged_data, message = validate_and_merge_datasets_byrows(datasets_to_merge)
        if merged_data is None:
            return jsonify({'status': 'error', 'message': message}), 400

        if not isinstance(merged_data, pd.DataFrame):
            try:
                merged_data = pd.DataFrame(merged_data)
            except Exception as e:
                error_message = f"Error converting merged_data to DataFrame: {str(e)}"
                return jsonify({'status': 'error', 'message': error_message}), 500

        # Enforce integer columns if needed
        merged_data = enforce_integer_columns(merged_data)

        # Get the desired file format from query parameters
        file_format = request.args.get('format', 'csv').lower()

        # Create file-like object
        output_buffer = io.BytesIO()

        if file_format == 'csv':
            merged_data.to_csv(output_buffer, index=False, float_format='%.0f')
            content_type = 'text/csv'
            file_extension = 'csv'
        elif file_format == 'xlsx':
            merged_data.to_excel(output_buffer, index=False)
            content_type = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
            file_extension = 'xlsx'
        elif file_format == 'json':
            json_data = merged_data.to_dict(orient='records')
            output_buffer.write(json.dumps(json_data).encode())
            content_type = 'application/json'
            file_extension = 'json'
        else:
            return jsonify({'status': 'error', 'message': 'Unsupported file format'}), 400

        # Seek to the beginning of the buffer
        output_buffer.seek(0)

        # Create the response
        response = make_response(output_buffer.getvalue())
        response.headers["Content-Disposition"] = f"attachment; filename=merged_dataset.{file_extension}"
        response.headers["Content-type"] = content_type

        return response

    except Exception as e:
        return jsonify({'status': 'error', 'message': str(e)}), 500      
    
