<!--
  ~ Copyright (c) 2024 Centre for Research and Technology Hellas (CERTH) / Information Technologies Institute (ITI)
  ~ 
  ~ Permission is hereby granted, free of charge, to any person obtaining a copy of
  ~ this software and associated documentation files (the "Software"), to deal in
  ~ the Software without restriction, including without limitation the rights to
  ~ use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
  ~ the Software, and to permit persons to whom the Software is furnished to do so,
  ~ subject to the following conditions:
  ~ 
  ~ The above copyright notice and this permission notice shall be included in all
  ~ copies or substantial portions of the Software.
  ~ 
  ~ THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  ~ IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
  ~ FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
  ~ COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
  ~ IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
  ~ CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
  ~ 
  ~ SPDX-License-Identifier: MIT
  ~ 
  ~ Contributors:
  ~       Nikolaos Tepelidis - Author
  ~       Vasileios Siopidis - Couthor
  ~       Georgios Nikolaidis - Couthor
  ~       Konstantinos Votis - Couthor
-->

<h2>Description </h2>

# Data-product-composition

![architecture_DPComp](/docs/figures/arch_extended_DATAPRODUCTCOMPOSITION.png)

Data product composition tool is responsible for accepting one or more datasets as input and let the user to merge them into one file by comparing the rows/columns or to delete/retain specific columns. More specifically, the tool can recognise one or multiple files for input by the user,  can analyze the imported dataset/s by giving info such as file name, column names, type of each field, number of entries for each field, size of the dataset. ALso can compare two or more datasets by columns/rows and merge them into one dataset if certain criteria are met and allows the user to select which columns of the imported dataset/s wants to delete/retain and merged the datasets into one file. In both situations the user then can retrieve the merged file. 

The following schema illustrates a systematic process for implementing a data sharing tool, as flow diagram.

![Alt text](/docs/figures/data_composition_tool_workflow_diagram.png)


<h2>Installation and requirements </h2>

<h4>Programming Languages </h4>

For this tool we will use Python as primary language to implement the functionalities. It is not necessary to have it installed in order to test the project on your local environment. For that purpose, the project uses Docker for a containerized execution.
Libraries

For this tool we will use the libraries that mentioned below:

•	[Pandas](https://pandas.pydata.org/): Python Data Analysis Library.

•	[Flask](https://flask.palletsprojects.com/en/3.0.x/): Flask is a lightweight WSGI web application framework. It is designed to make getting started quick and easy, with the ability to scale up to complex applications.

•	[Io](https://docs.python.org/3/library/io.html): Core tools for working with streams

<h4>Running the Data product composition tool  </h4>

The data product composition tool is a web-based tool based on python programming language. To run the data product composition tool firstly, you need to download the repo. The repo includes Dockerfile and .yaml file, which are required to run the tool and a “requirements.txt” file, which includes the necessary libraries to run the code. 

First you need to create a shared network (optional) because the data product composition tool is communicated also with other tools for example anonymization tool. If you don't need any of the other tools that data product communicated with there is no need to create a network. You can use this command to create a network:

`docker network create shared_network`

You can build and run the code by navigating to the path of data product composition tool through cmd and type: 

`docker-compose up –-build`

If for some reason you want to delete the shared_network you can use the following command: 

`docker network rm shared_network`

Also you will need to configure the database through pgadmin if you want to see the saved data. You can go to pgadmin and click on register server. Then you will need to give a name of the server and in connection tab you should write these:

Hostname: localhost
Port: 25432
Password: 1234
Maintenance database: postgres
username: postgres

Note: In order to run docker command you need to install docker on your operating system.
After code build successfully you can access swagger documentation of data product composition tool through any web browser by typing: 

http://127.0.0.1:5001/product/


<h4>Databases </h4>

The database that used in the tool is PostgreSql 

<h2>Roadmap</h2>

![Alt text](/docs/figures/roadmap_data_product.png)

<h2>Indicative endpoints</h2>

•	Description: This endpoint handles dataset/s upload

```
Input:
  HTTP Method: Post 

  Endpoint: ‘api/upload_dataset’ 

  Headers: Content-Type: application/json 

  Body: {"username": "example_user", "email": "user@example.com", "file": "dataset.csv"} 

Response:
{“Status Code”: “200 Dataset uploaded successfully”}
```

•	Description: This endpoint gives more information about users uploaded, merged or filtered dataset/s 

```
Input:
  HTTP Method: Post 

  Endpoint: ‘api/analyze_datasets’ 

  Headers: Content-Type: application/json 

  Body: {"file": "dataset.csv"} 

Response:
{"csv_file": "dataset.csv", "entries_count", "field_names", "field_types", "file_size"}
```


•	Description: Those endpoints handles the process of aggregating data by columns/rows.

```
Input:
HTTP Method: Post 

  Endpoint: ‘api/merge_datasets_bycolumns’ 

  Headers: Content-Type: application/json 

  Body: {"file1": "dataset1.csv", "file2": "dataset2.csv" e.t.c.}

Response:

{“Status Code”: “200 Datasets merged successfully”}
```

```
Input:
HTTP Method: Post 

  Endpoint: ‘api/merge_datasets_byrows’ 

  Headers: Content-Type: application/json 

  Body: {"file1": "dataset1.csv", "file2": "dataset2.csv" e.t.c.}

Response:

{“Status Code”: “200 Datasets merged successfully”}
```



•	Description: Those endpoints handles the process of retrieving data from the user for aggregate data by columns/rows module. 
```
Input:
HTTP Method: Get 

  Endpoint: ‘api/download_merged_dataset_bycolumns’ 

  Headers: Content-Type: application/json 

  Body: {"merged_dataset": "merged_dataset", "format": "csv,xlsx,json"}
```

```
Input:
HTTP Method: Get 

  Endpoint: ‘api/download_merged_dataset_byrows’ 

  Headers: Content-Type: application/json 

  Body: {"merged_dataset": "merged_dataset", "format": "csv,xlsx,json"}
```

•	Description: Those endpoints handles the process of delete/retain data

```
Input:
HTTP Method: Post 

  Endpoint: ‘api/delete_columns’ 

  Headers: Content-Type: application/json 

  Body: ["dataset1.csv", “columns”], ["dataset2.csv", “columns”]

Response:

{“Status Code”: “200 Columns deleted successfully”}
```

```
Input:
HTTP Method: Post 

  Endpoint: ‘api/retain_columns’ 

  Headers: Content-Type: application/json 

  Body: ["dataset1.csv", “columns”], ["dataset2.csv", “columns”]

Response:

{“Status Code”: “200 Columns retained successfully”}
```

•	Description: This endpoint handles the process of filtering a dataset 

```
Input:
HTTP Method: Post 

  Endpoint: ‘api/filter_columns’ 

  Headers: Content-Type: application/json 

  Body: {"file": "dataset1.csv", ["column": "column_name1", "value": "value1"], ["column": "column_name2", "value": "value2"] }

Response:

{"filtered_data": "...."}
```

•	Description: This endpoint handles the process of saving the data product into the database

```
Input:
HTTP Method: Post 

  Endpoint: ‘api/save_data_product’ 

  Headers: Content-Type: application/json 

  Body: {"name_of_the_file": "myfile", "keywords": "data, composition", "description": "this is a file"}

Response:

{“Status Code”: “200 Data product saved successfully”}

```

•	Description: This endpoint handles the process of searching metadata taken from the database

```
Input:
HTTP Method: Get 

  Endpoint: ‘api/search_metadata’ 

  Headers: Content-Type: application/json 

  Body: {"file_name": "myfile", "keywords": "data, composition", "description": "this is a file", "file_size": "50-100", "data_created": "2024-09-08-2024-09-25" ....}

Response:

{"metadata_entries": "...."}

```